#include <stdio.h>
#include <stdlib.h>

#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN    "\x1b[36m"
#define RESET   "\x1b[0m"

#define ASSERTION_HANDLER_COLOR CYAN

#define  FILL(COLOR,TEXT) COLOR TEXT RESET

#define CLEAR_STDIN_LINE() while (getchar() != '\n')

static double __hash_fun(const char* file_name, const int line, const char* message){
	char* cursor = (char*)message;
	double hash = 0.0;
	double buff = 0.0;
	int iterations = 30;
	while (*cursor != '\0' && iterations--){
		buff += *cursor;
		cursor++;
	}
	
	if (buff == 0.0){
		buff = file_name[0] + file_name[1] + file_name[2];
	}
		
	buff = line / buff;
	
	cursor = (char*)file_name;
	double addiction = buff;
	while(*cursor != '\0'){
		hash += *cursor;
		hash *= addiction;
		addiction += buff;
		cursor++;
	}
	return hash;
}

void __assert_debug(int condition, const char* file, const int line, const char* message){
	
	static struct{
		double* arr;
		unsigned len;
	} skip_asserts = {NULL, 0};
	
	static int skip;
	
	if (condition || skip)
		return;
	
	double hash = __hash_fun(file, line, message);
	
	for (int i = 0; i < skip_asserts.len; i++)
		if (skip_asserts.arr[i] == hash)
			return;
	
	fprintf(stderr, FILL(RED, "Assertion") FILL(BLUE, " %s %d:") " %s\n", file, line, message);
	char ans;
	
	fputs(FILL(ASSERTION_HANDLER_COLOR, "Assertion handler (h for help)"), stderr);
	fprintf(stderr, FILL(ASSERTION_HANDLER_COLOR,"\n(h/s/c/e): "));
	ans = getchar();
	CLEAR_STDIN_LINE();
	while(ans != 's' && ans != 'c' && ans != 'e'){
		if (ans == 'h'){
			fputs(FILL(ASSERTION_HANDLER_COLOR,"\th - print help\n"), stderr);
			fputs(FILL(ASSERTION_HANDLER_COLOR,"\ts - skip this assert\n"), stderr);
			fputs(FILL(ASSERTION_HANDLER_COLOR,"\tc - continue work without asserts\n"), stderr);
			fputs(FILL(ASSERTION_HANDLER_COLOR,"\te - end program work\n"), stderr);
		}
		else
			fputs(FILL(YELLOW, "Incorrect input answer. Please try again.\n"), stderr);
		fprintf(stderr, FILL(ASSERTION_HANDLER_COLOR,"\n(h/s/c/e): "));
		ans = getchar();
		CLEAR_STDIN_LINE();
	}
	
	switch(ans){
		case 's':
			if (skip_asserts.arr == NULL){
				if ((skip_asserts.arr = malloc(sizeof(long))) == NULL)
					return;
				else{
					*skip_asserts.arr = hash;
					skip_asserts.len++;
				}
			}
			else{
				double* buff_p = realloc(skip_asserts.arr, sizeof(double) * ++skip_asserts.len);
				if (buff_p == NULL){
					free(skip_asserts.arr);
					fprintf(stderr, "Error memory allocate in assert!\n");
					return;
				}
				skip_asserts.arr = buff_p;
				skip_asserts.arr[skip_asserts.len - 1] = hash;
			}
			break;
		case 'c':
			skip = 1;
			break;
		case 'e':
			if (skip_asserts.arr != NULL)
				free(skip_asserts.arr);
			exit(0);
			break;
	}
}
