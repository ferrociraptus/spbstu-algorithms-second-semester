#include "Checker.h"
#include "Assert.h"

void check_pos_val(int val){
	assert(0 < val, "Value is not positive");
}

void check_neg_val(int val){
	assert(0 > val, "Value is not negative");
}
