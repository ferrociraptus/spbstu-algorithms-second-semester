#ifndef ASSERT_LIB_H
#define ASSERT_LIB_H

#if !defined(ASSERT_DEBUG) && !defined(ASSERT_RELEASE)
#define ASSERT_RELEASE
#endif

#ifdef assert 
#undef assert
#endif

#ifdef ASSERT_DEBUG
#undef ASSERT_RELEASE
#endif

#ifdef ASSERT_DEBUG
	#define assert(C,M) __assert_debug(!!(C), __FILE__, __LINE__, M)//
	extern void __assert_debug(int, const char*, const int, const char*);
#endif //#ifdef ASSERT_DEBUG

#ifdef ASSERT_RELEASE
	#define assert(C,M)
#endif //#ifdef ASSERT_RELEASE


#ifndef _STDIO_H
#include <stdio.h>
#endif
#define fatalAssert(C,M,V) {if (!(C)){fputs(M, stderr); return (V);}}

// #define stat(exp) (void)(!!(exp)||(assert(__FILE__, __LINE__), 0))

#define name_gen(msg, line, file) assertion_message_ ## msg ## _line_ ## line ## _in_file_ ## file
#define static_ass(msg,exp) int assertion_message_ ## msg ## __LINE__ ## line ## _in_file_ ## __FILE__[!!(exp) ? 1: -1]


#endif //ASSERT_LIB_H
