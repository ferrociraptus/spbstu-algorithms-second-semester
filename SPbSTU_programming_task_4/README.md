# Arcanoid

Write the simple game as Arcanoid.

Project completed with GTK3+ and C.

Looks like:

Menu:

![Menu](doc_src/menu.png)

Level:

![Level](doc_src/level.png)

Start of game:

![Start](doc_src/start.png)

Game process:

![Game process](doc_src/process.png)