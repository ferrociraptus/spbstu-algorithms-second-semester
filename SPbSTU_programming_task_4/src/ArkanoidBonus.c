#include <stdlib.h>
#include "ArkanoidBonus.h"
#include "Timers.h"

static BonusHandler activate_bonus_handler;
static BonusHandler deactivate_bonus_handler;

void __destroy_bonus(Bonus* bonus){
	if (bonus == NULL)
		return;
	
	if (bonus->image != NULL)
		cairo_surface_destroy(bonus->image);
	
	free(bonus);
}

void __bonus_draw(Bonus* bonus, cairo_t* cr){
	double x1, y1, x2, y2;
    cairo_t* img = cairo_create(bonus->image);
    cairo_clip_extents(img, &x1, &y1, &x2, &y2);
    cairo_destroy(img);
    
    double height = x2 - x1;
    double width = y2 - y1;
    Point draw_point = {
        .x = bonus->widget.center_position.x - height / 2,
        .y = bonus->widget.center_position.y - width / 2
    };
    cairo_set_source_surface(cr, bonus->image, draw_point.x, draw_point.y);
    cairo_paint(cr);
}

Bonus* bonus_new(ArkanoidBonusType type, double speed){
	Bonus* bonus = (Bonus*)malloc(sizeof(Bonus));
	if (bonus == NULL)
		return NULL;
	bonus->speed = speed;
	bonus->type = type;
	bonus->image = NULL;
	
	bonus->widget = mcw_widget_create_base();
	bonus->widget.destroy_widget = MCW_DESTROY_WIDGET_HANDLER(__destroy_bonus);
	bonus->widget.draw_mywidget = MCW_WIDGET_DRAW_FUN(__bonus_draw);
	bonus->widget.size = (Size){BONUS_RADIUS*2, BONUS_RADIUS*2};
	
	switch(bonus->type){
		case INCREASE_PLATFORM_LEN:
			bonus->image = cairo_image_surface_create_from_png(INCREASE_PLATFORM_BONUS_IMAGE);
			break;
		case CREATE_NEW_BALL:
			bonus->image = cairo_image_surface_create_from_png(CREATE_NEW_BALL_BONUS_IMAGE);
			break;
	}
	return bonus;
}

static void __deactivate_bonus(Bonus* bonus){
	(*deactivate_bonus_handler)(bonus->type);
	mcw_widget_destroy(MCW_WIDGET(bonus));
}

void bonus_activate(Bonus* bonus){
	(*activate_bonus_handler)(bonus->type);
	switch(bonus->type){
		case INCREASE_PLATFORM_LEN:
			if (timer_add_time("increase-platform-len", INCREASE_PLATFORM_BONUS_TIME) != 1)
				timer_init_new("increase-platform-len", INCREASE_PLATFORM_BONUS_TIME, TIMER_HANDLER(__deactivate_bonus), bonus);
			break;
		case CREATE_NEW_BALL:
// 			timer_init_new("create_new_ball", CREATE_NEW_BALL_BONUS_TIME,
// 						   TIMER_HANDLER(__deactivate_bonus), bonus);
			break;
	}
}

double bonus_get_speed(Bonus* bonus){
	return bonus->speed;
}

void bonus_set_speed(Bonus* bonus, double speed){
	bonus->speed = speed;
}

void bonus_update_position(Bonus* bonus){
	bonus->widget.center_position = point_sum(bonus->widget.center_position, point_create(0, bonus->speed));
}

void bonus_set_activator(BonusHandler apply_bonus){
	activate_bonus_handler = apply_bonus;
}

void bonus_set_deactivator(BonusHandler cancel_bonus){
	deactivate_bonus_handler = cancel_bonus;
}
