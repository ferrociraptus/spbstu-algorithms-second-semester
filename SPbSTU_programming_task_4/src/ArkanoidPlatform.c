#include <stdlib.h>
#include <cairo.h>

#include "ArkanoidPlatform.h"
#include "McwSystem.h"

struct _platform{
    McwWidget widget;
    cairo_surface_t* image;
};

void __platform_destroy(Platform* platform){
    if (platform == NULL)
		return;
	free(platform);
	cairo_surface_destroy(platform->image);
}

void __platform_draw(Platform* widget, cairo_t* cr){
    Point draw_point = {
        .x = widget->widget.center_position.x - widget->widget.size.width/2,
        .y = widget->widget.center_position.y - widget->widget.size.height/2
    };
    cairo_set_source_surface(cr, widget->image, draw_point.x, draw_point.y);
    cairo_paint(cr);
}

Platform* platform_new(){
    Platform* platform = (Platform*)malloc(sizeof(Platform));
    platform->widget = mcw_widget_create_base();
    platform->widget.destroy_widget = MCW_DESTROY_WIDGET_HANDLER(__platform_destroy);
    platform->widget.center_position.x = (double)MIN_WINDOW_SIZE_WEIGHT_PX / 2.0;
    platform->widget.center_position.y = PLATFORM_HEIGHT_POSITION;
    platform->image = cairo_image_surface_create_from_png(PLATFORM_IMAGE);
    platform->widget.draw_mywidget = MCW_WIDGET_DRAW_FUN(__platform_draw);
    platform->widget.size.height = PLATFORM_HEIGT;
    platform->widget.size.width = PLATFORM_WIDTH;
    
    return platform;
}

void platform_make_long(Platform* platform){
	platform->widget.size.width = LONG_PLATFORM_WIDTH;
	cairo_surface_destroy(platform->image);
	platform->image = cairo_image_surface_create_from_png(LONG_PLATFORM_IMAGE);
}

void platform_make_normal(Platform* platform){
	platform->widget.size.width = PLATFORM_WIDTH;
	cairo_surface_destroy(platform->image);
	platform->image = cairo_image_surface_create_from_png(PLATFORM_IMAGE);
}
