#include "McwWidget.h"
#include <stdlib.h>
#include <string.h>

void mcw_empty_handler(void* widget, void* data){
    return;
};

static void _mcw_widget_destroy(McwWidget* widget){
    free(widget);
}; 

McwWidget mcw_widget_create_base(){
    McwWidget widget;
    widget.is_visible = TRUE;
    widget.size.height = -1;
    widget.size.width = -1;
    widget.autosize.height = AUTO_MAX;
    widget.autosize.width = AUTO_MAX;
    widget.destroy_widget = MCW_DESTROY_WIDGET_HANDLER(_mcw_widget_destroy);
    widget.parent = MCW_ROOT;
    widget.draw_mywidget = MCW_WIDGET_SIGNALS_HANDLER(mcw_empty_handler);
    
    widget.__system_calls.clicked.signal = MCW_WIDGET_SIGNALS_HANDLER(mcw_empty_handler);
    widget.__system_calls.release.signal = MCW_WIDGET_SIGNALS_HANDLER(mcw_empty_handler);
    widget.__system_calls.pressed_up.signal = MCW_WIDGET_SIGNALS_HANDLER(mcw_empty_handler);
    widget.__system_calls.pressed_down.signal = MCW_WIDGET_SIGNALS_HANDLER(mcw_empty_handler);
    widget.__system_calls.pressed_enter.signal = MCW_WIDGET_SIGNALS_HANDLER(mcw_empty_handler);
    widget.__system_calls.mouse_moved.signal = MCW_WIDGET_SIGNALS_HANDLER(mcw_empty_handler);
    
    widget.signals.clicked.signal = MCW_WIDGET_SIGNALS_HANDLER(mcw_empty_handler);
    widget.signals.release.signal = MCW_WIDGET_SIGNALS_HANDLER(mcw_empty_handler);
    widget.signals.pressed_up.signal = MCW_WIDGET_SIGNALS_HANDLER(mcw_empty_handler);
    widget.signals.pressed_down.signal = MCW_WIDGET_SIGNALS_HANDLER(mcw_empty_handler);
    widget.signals.pressed_enter.signal = MCW_WIDGET_SIGNALS_HANDLER(mcw_empty_handler);
    widget.signals.mouse_moved.signal = MCW_WIDGET_SIGNALS_HANDLER(mcw_empty_handler);
    
    memset(widget.indificator, '\0', INDIFICATOR_SIZE);
    return widget;
};

McwWidget* mcw_widget_new(){
    McwWidget* widget = (McwWidget*)malloc(sizeof(McwWidget));
    *widget = mcw_widget_create_base();
    return widget;
};

void mcw_widget_destroy(McwWidget* widget){
    widget->destroy_widget(widget);
};

Size mcw_widget_get_size(McwWidget* widget){
    Size size;
    size.height = widget->size.height;
    size.width = widget->size.width;
    return size;
};

void mcw_widget_set_size(McwWidget* widget, Size new_size){
    if (new_size.width != -1){
        widget->size.height = new_size.height;
        widget->autosize.height = AUTO_OFF;
    }
    if (new_size.height != -1){
        widget->size.width = new_size.width;
        widget->autosize.width = AUTO_OFF;
    }
};

Point mcw_widget_get_center(McwWidget* widget){
    return widget->center_position;
};

void mcw_set_widget_center_position(McwWidget* widget, Point new_pos){
    widget->center_position.x = new_pos.x;
    widget->center_position.y = new_pos.y;
};

void mcw_widget_set_parent(McwWidget* widget, void* parent){
    widget->parent = parent;
};

void mcw_widget_draw(McwWidget* widget, cairo_t* cr){
    if (mcw_widget_is_visible(widget))
        (*((McwWidget*) widget)->draw_mywidget)(widget, cr);
};

void mcw_rise_signal(McwWidget* widget, McwWidgetSignal* signal){
    (*signal->signal)(widget, signal->signal_data);
};

void mcw_rise_position_signal(McwWidget* widget, McwWidgetSignal* signal, Point position){
    (*signal->signal)(widget, &position);
};

void mcw_widget_connect_signal_handler(McwWidgetSignal* signal, void (*handler) (void*, void*), void* data){
    signal->signal = handler;
    signal->signal_data = data;
};

void mcw_set_autosize_scale_for_widget(McwWidget* widget, AutosizeType width, AutosizeType height){
    widget->autosize.width = width;
    widget->autosize.height = height;
};

void mcw_set_widget_indificator(McwWidget* widget, char* id){
    memset(widget->indificator, '\0', INDIFICATOR_SIZE);
    strcpy(widget->indificator, id);
};

int mcw_widget_is_visible(McwWidget* widget){
    McwWidget* parent = widget->parent;
    while (parent != MCW_ROOT){
        if (parent->is_visible == FALSE)
            return -1;
        parent = parent->parent;
    }
    return 1;
};
