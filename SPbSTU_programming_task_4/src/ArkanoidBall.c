#include <stdlib.h>
#include <time.h>
#include <cairo.h>
#include <math.h>

#include "McwWidget.h"
#include "ArkanoidBall.h"
#include "Tail.h"

struct __arkanoid_ball{
    McwWidget widget;
    ArkanoidBallState state;
    unsigned radius;
    RVector move_vector;
    double speed;
    Tail* tail;
};

void __arkanoid_ball_draw(Ball* ball, cairo_t* cr){
    for (int i = void_list_len(ball->tail) - 1; i >= 0; i--){
        TailNode* node = void_list_get(ball->tail, i);
        ColorRGB color = node->color;
        cairo_set_source_rgb(cr, color.red, color.green, color.blue);
        cairo_arc(cr, node->position.x, node->position.y, node->radius, 0, 2*M_PI);
        cairo_fill(cr);
        cairo_stroke(cr);
    }
    return;
}


void __arkanoid_ball_destroy(Ball* ball){
    if (ball == NULL)
        return;
    
    tail_destroy(ball->tail);
    free(ball);
}

Ball* arkanoid_ball_new(unsigned length, RVector move_vector, Point start_position, ColorRange colors, double radius, double measure_diff){
    Ball* ball = (Ball*)malloc(sizeof(Ball));
    if (ball == NULL)
        return NULL;
    
    ball->tail = tail_new(length, start_position, colors, radius, measure_diff);
    
    if (ball->tail == NULL){
        free(ball);
        return NULL;
    }
    
    ball->state = ARKBALL_USUAL;
    ball->widget.center_position = start_position;
    ball->radius = radius;
    ball->speed = rvector_len(move_vector);
    
    srand(time(NULL));
    ball->move_vector = move_vector;
    
    ball->widget = mcw_widget_create_base();
    ball->widget.draw_mywidget = MCW_WIDGET_DRAW_FUN(__arkanoid_ball_draw);
    ball->widget.destroy_widget = MCW_DESTROY_WIDGET_HANDLER(__arkanoid_ball_destroy);
    
    return ball;
}

void arkanoid_ball_set_speed(Ball* ball, double speed){
    ball->move_vector = rvector_set_len(ball->move_vector, speed);
}

void arkanoid_ball_set_state(Ball* ball, ArkanoidBallState state){
    ball->state = state;
}

ArkanoidBallState arkanoid_ball_get_state(Ball* ball){
    return ball->state;
}

void arkanoid_ball_hit_handler(Ball* ball, HitType hit, RVector change_vector){
    switch(hit){
        case VERTICAL_HIT:
            ball->move_vector.x *= -1.0;
            break;
        case HORISONTAL_HIT:
            ball->move_vector.y *= -1.0;
            break;
        case NO_HIT:
            return;
    }
    if (rvector_is_zero(change_vector) <= 0)
        ball->move_vector = rvector_set_len(rvector_sum(ball->move_vector,change_vector), ball->speed);
}

void arkanoid_ball_update_position(Ball* ball){
    tail_shift_head(ball->tail, ball->move_vector);
    ball->widget.center_position = ((TailNode*)void_list_get(ball->tail, 0))->position;
}

void arkanoid_ball_set_position(Ball* ball, Point new_pos){
    for (int i = 0; i < void_list_len(ball->tail); i++)
        ((TailNode*)void_list_get(ball->tail, i))->position = new_pos;
    mcw_set_widget_center_position(&ball->widget, new_pos);
}

double arkanoid_ball_get_radius(Ball* ball){
    return ball->radius;
}

void arkanoid_ball_set_move_vector(Ball* ball, RVector vector){
    ball->move_vector = rvector_set_len(vector, ball->speed);
}
