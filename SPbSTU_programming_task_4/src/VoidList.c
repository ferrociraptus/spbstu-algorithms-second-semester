#include "VoidList.h"
#include <stdlib.h>

#define abs(A) (((A) < 0) ? -(A) : (A))

typedef struct _list_node {
    struct _list_node* next_node;
    struct _list_node* previous_node;
    void* item;
} McwWidgetListNode;

static McwWidgetListNode* __void_list_node_new(void* item) {
    McwWidgetListNode *node = (McwWidgetListNode*) malloc(sizeof(McwWidgetListNode));
    if (node == NULL)
        return NULL;
    node->item = item;
    return node;
}

inline static void __void_list_node_destroy(McwWidgetListNode *node, Destructor item_destructor){
    if (node == NULL)
        return;
    (*item_destructor)(node->item);
    free(node);
}

struct _mcw_void_list{
    unsigned long len;
    unsigned long current_index;
    McwWidgetListNode* current_node;
    Destructor item_destructor;
};

static void __remove_all_nodes(VoidList* list, McwWidgetListNode* node){
    if (node != list->current_node->previous_node)
        __remove_all_nodes(list, node->next_node);
    __void_list_node_destroy(node, list->item_destructor);
};

static void __go_to_index(VoidList* list, unsigned index){
    
    if (index > list->len)
        index = list->len-1;
    if (index < 0)
        index = 0;
    
    int diff1 = list->current_index - index;
    int diff2 = -index - list->len + list->current_index;
    int diff = (abs(diff1) < abs(diff2)) ? diff1 : diff2;
    list->current_index = index;
    for (int i = 0; i < abs(diff); i++)
        if (diff < 0)
            list->current_node = list->current_node->next_node;
        else
            list->current_node = list->current_node->previous_node;
}

static void __empty_destructor(void* val){
    return;
}


VoidList* void_list_new(int nodes_amount, Destructor items_destructor){
    VoidList* list = (VoidList*)malloc(sizeof(VoidList));
    if (list == NULL)
        return NULL;
    list->len = 0;
    list->current_index = 0;
    list->current_node = NULL;
    
    if (items_destructor == NULL)
        list->item_destructor = __empty_destructor;
    else
        list->item_destructor = items_destructor;
    
    McwWidgetListNode *node;

    if (nodes_amount > 0){
        node = __void_list_node_new(NULL);
        if (node == NULL)
            return NULL;
        
        list->current_node = node;
        node->previous_node = node;
        node->next_node = node;
        list->len++;
        for (int i = 0; i < nodes_amount - 1; i++){
            node->next_node = __void_list_node_new(NULL);
            if (node->next_node == NULL){
                node->next_node = list->current_node;
                void_list_destroy(list);
                return NULL;
            }
            node->next_node->previous_node = node;
            node = node->next_node;
            list->len++;
        }
        node->next_node = list->current_node;
        list->current_node->previous_node = node;
    }
    return list;
};

void void_list_destroy(VoidList* list){
    if (list == NULL)
        return;
    void_list_clear(list);
    free(list);
}

void void_list_clear(VoidList* list){
    if (list->current_node != NULL)
        __remove_all_nodes(list, list->current_node->next_node);
    list->len = 0;
    list->current_index = 0;
    list->current_node = NULL;
}

int void_list_insert(VoidList* list, int index, void* item){
    McwWidgetListNode* new_node = __void_list_node_new(item);
    if (new_node == NULL || index < 0)
        return -1;
    if (index > list->len)
        index = list->len;
    
    if (list->len == 0){
        new_node->next_node = new_node;
        new_node->previous_node = new_node;
        list->current_node = new_node;
    }
    else{
        __go_to_index(list, index);
        new_node->next_node = list->current_node;
        new_node->previous_node = list->current_node->previous_node;
        new_node->previous_node->next_node = new_node;
        list->current_node->previous_node = new_node;
    }
    list->current_index++;
    list->len++;
    return 1;
}

int void_list_append(VoidList* list, void* item){
    return void_list_insert(list, list->len, item);
}

void* void_list_get(VoidList* list, int index){
    __go_to_index(list, index);
    return list->current_node->item;
}

void void_list_set(VoidList* list, int index, void* item){
    __go_to_index(list, index);
    if (list->current_node->item != NULL)
        (*list->item_destructor)(list->current_node->item);
    list->current_node->item = item;
}

void void_list_remove(VoidList* list, int index){
    __go_to_index(list, index);
    list->len--;
    McwWidgetListNode* del_node = list->current_node;
    del_node->previous_node->next_node = del_node->next_node;
    del_node->next_node->previous_node = del_node->previous_node;
    if (list->len > 0){
        list->current_node = del_node->next_node;
        list->current_index = list->current_index % list->len;
    }
    else{
        list->current_node = NULL;
        list->current_index = 0;
    }
    __void_list_node_destroy(del_node, list->item_destructor);
}

void* void_list_pop(VoidList* list, int index){
    __go_to_index(list, index);
	list->len--;
    McwWidgetListNode* del_node = list->current_node;
    del_node->previous_node->next_node = del_node->next_node;
    del_node->next_node->previous_node = del_node->previous_node;
    if (list->len > 0){
        list->current_node = del_node->next_node;
        list->current_index = list->current_index % list->len;
    }
    else{
        list->current_node = NULL;
        list->current_index = 0;
    }
    void* item = del_node->item;
    __void_list_node_destroy(del_node, __empty_destructor);
    return item;
}

unsigned long void_list_len(VoidList* list){
    return list->len;
}
