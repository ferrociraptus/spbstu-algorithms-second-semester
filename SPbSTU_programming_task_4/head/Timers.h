#ifndef TIMERS_H
#define TIMERS_H

typedef void (*TimerHandler)(void*);
#define TIMER_HANDLER(A) (TimerHandler)(A)

signed timer_init_new(char* indificator, unsigned long tiks, TimerHandler handler, void* data);
void timer_tick();
void timer_destroy_all();
int timer_set_time(const char* indificator, unsigned tiks);
int timer_add_time(const char* indificator, unsigned tiks);
void timer_block();
int timer_is_active();
void timer_activate();

#endif
