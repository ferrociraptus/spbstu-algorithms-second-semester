#ifndef ARKANOID_BRICK_H
#define ARKANOID_BRICK_H

#include <stdlib.h>
#include <string.h>
#include <cairo.h>
#include "RVectorStruct.h"
#include "McwWidget.h"

#define BRICK_HEIGHT 25.0
#define BRICK_WIDTH 70.0

#define BRICK_STRONG_FULL_IMAGE "./icons/strong_brick_0.png"
#define BRICK_STRONG_DAMAGED_IMAGE "./icons/strong_brick_1.png"
#define BRICK_STRONG_RUINS_IMAGE "./icons/strong_brick_2.png"
#define BRICK_USUAL_IMAGE "./icons/usual_brick.png"

typedef enum {BRICK_USUAL, BRICK_STRONG} BrickType;

typedef struct _brick{
    McwWidget widget;
    BrickType type;
    unsigned lives;
    cairo_surface_t* image;
} Brick;

Brick* brick_new(BrickType type, Point center_pos);
void brick_destroy(Brick* brick);
void brick_hit(Brick* brick);
int brick_is_broken(Brick* brick);

#endif
