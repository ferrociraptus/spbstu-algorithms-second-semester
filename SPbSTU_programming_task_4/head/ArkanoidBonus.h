#ifndef ARKANOID_BONUS_H
#define ARKANOID_BONUS_H

#include "McwWidget.h"

#define BONUS_RADIUS 20

#define INCREASE_PLATFORM_BONUS_IMAGE "icons/enlarge_panel_bonus.png"
#define CREATE_NEW_BALL_BONUS_IMAGE "icons/new_ball_bonus.png"

#define INCREASE_PLATFORM_BONUS_TIME 6000
#define CREATE_NEW_BALL_BONUS_TIME 50

typedef enum {INCREASE_PLATFORM_LEN, CREATE_NEW_BALL} ArkanoidBonusType;

typedef struct{
	McwWidget widget;
	ArkanoidBonusType type;
	double speed;
	cairo_surface_t* image;
} Bonus;

typedef void (*BonusHandler)(ArkanoidBonusType);

Bonus* bonus_new(ArkanoidBonusType type, double speed);
void bonus_activate(Bonus* bonus);
double bonus_get_speed(Bonus* bonus);
void bonus_set_speed(Bonus* bonus, double speed);
void bonus_update_position(Bonus* bonus);

void bonus_set_activator(BonusHandler apply_bonus);
void bonus_set_deactivator(BonusHandler cancel_bonus);

#endif
