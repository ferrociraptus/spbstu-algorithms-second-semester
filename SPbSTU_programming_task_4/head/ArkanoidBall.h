#ifndef ARKANOID_BALL_H
#define ARKANOID_BALL_H

#include "ColorRangeStruct.h"
#include "RVectorStruct.h"

typedef struct __arkanoid_ball Ball;
typedef enum {ARKBALL_USUAL, ARKBALL_FIRE_BALL} ArkanoidBallState;
typedef enum {NO_HIT, VERTICAL_HIT, HORISONTAL_HIT} HitType;

Ball* arkanoid_ball_new(unsigned length, RVector move_vector, Point start_position, ColorRange colors, double radius, double measure_diff);
void arkanoid_ball_set_speed(Ball* ball, double speed);
void arkanoid_ball_set_state(Ball* ball, ArkanoidBallState state);
ArkanoidBallState arkanoid_ball_get_state(Ball* ball);
void arkanoid_ball_hit_handler(Ball* ball, HitType hit, RVector change_vector);
void arkanoid_ball_update_position(Ball* ball);
double arkanoid_ball_get_radius(Ball* ball);
void arkanoid_ball_set_position(Ball* ball, Point new_pos);
void arkanoid_ball_set_move_vector(Ball* ball, RVector vector);
#endif
