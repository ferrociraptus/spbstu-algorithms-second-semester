#include <stdlib.h>
#include <gtk/gtk.h>
#include "McwVerticalLayout.h"
#include "McwButton.h"
#include "McwWidget.h"
#include "McwWidgetsList.h"


struct _McwVerticalLayout {
    McwWidget widget;
    McwWidgetList* layout_widgets;
    int margin;
    int selected_button_index;
    unsigned trigger_mode:1;
};

void __mcw_layout_set_next(McwVerticalLayout* layout){
    layout->selected_button_index--;
    if (layout->selected_button_index < 0)
        layout->selected_button_index = get_mcw_widget_list_length(layout->layout_widgets) - 1;
};

void __mcw_layout_set_previous(McwVerticalLayout* layout){
    layout->selected_button_index++;
    if (get_mcw_widget_list_length(layout->layout_widgets) == layout->selected_button_index)
        layout->selected_button_index = 0;
};

void __mcw_layout_press_current_button(McwVerticalLayout* layout){
    if (!layout->trigger_mode)
        return;
    McwWidget* pressed_widget = MCW_WIDGET(
        get_mcw_widget_from_mcw_widget_list(layout->layout_widgets ,layout->selected_button_index));
    mcw_rise_signal(pressed_widget, &pressed_widget->__system_calls.clicked);
        
};

void __mcw_layout_unpress_current_button(McwVerticalLayout* layout){
    if (!layout->trigger_mode)
        return;
    McwWidget* pressed_widget = MCW_WIDGET(
        get_mcw_widget_from_mcw_widget_list(layout->layout_widgets ,layout->selected_button_index));
    mcw_rise_signal(pressed_widget, &pressed_widget->__system_calls.release);
};

void draw_myvertical_layout(McwVerticalLayout* layout, cairo_t* cr){
    
    if (layout->layout_widgets == NULL)
        return;
    mcw_update_vertical_layout(layout);
    
    if (layout->trigger_mode == FALSE)
        return;
    
    for (int i = 0; i < get_mcw_widget_list_length(layout->layout_widgets); i++){
        if (i == layout->selected_button_index){
            McwWidget* widget = MCW_WIDGET(
            get_mcw_widget_from_mcw_widget_list(layout->layout_widgets, i));
            if (widget == NULL)
                break;
            Point widget_middle = mcw_get_widget_center_position(widget);
            Size widget_size = mcw_get_widget_size(widget);
            cairo_set_source_rgb(cr, SELECT_FRAME_COLOR );
            cairo_rectangle(cr, widget_middle.x - widget_size.width/2 - SELECT_FRAME_MARGIN, widget_middle.y - widget_size.height/2 - SELECT_FRAME_MARGIN, widget_size.width + 2 * SELECT_FRAME_MARGIN, widget_size.height + 2 * SELECT_FRAME_MARGIN );
            cairo_fill(cr);
            cairo_stroke(cr);
        }
            
            
        mcw_draw_widget(MCW_WIDGET(
            get_mcw_widget_from_mcw_widget_list(layout->layout_widgets, i)), cr);
    }
};

void _mcw_destroy_vertical_layout(McwVerticalLayout* layout){
    if (layout == NULL)
        return;
    free(layout);
};

McwVerticalLayout* mcw_new_vertical_layout(){
    McwVerticalLayout* layout = (McwVerticalLayout*)malloc(sizeof(McwVerticalLayout));
    if (layout == NULL)
        return NULL;
    layout->trigger_mode = 0;
    layout->widget = mcw_create_base_widget();
    layout->margin = DEFAULT_MYBUTTON_LAYOUT_MARGIN;
    layout->layout_widgets = NULL;
    layout->widget.draw_mywidget = MCW_WIDGET_DRAW_FUN(draw_myvertical_layout);
    layout->widget.destroy_widget = MCW_DESTROY_WIDGET_HANDLER(_mcw_destroy_vertical_layout);
    layout->selected_button_index = 0;
    layout->widget.__system_calls.pressed_down.signal = MCW_WIDGET_SIGNALS_HANDLER(__mcw_layout_set_previous);
    layout->widget.__system_calls.pressed_up.signal = MCW_WIDGET_SIGNALS_HANDLER(__mcw_layout_set_next);
    layout->widget.__system_calls.pressed_enter.signal = MCW_WIDGET_SIGNALS_HANDLER(__mcw_layout_press_current_button);
    layout->widget.__system_calls.release.signal = MCW_WIDGET_SIGNALS_HANDLER(__mcw_layout_unpress_current_button);
    return layout;
};

void mcw_add_widget_to_vertical_layout(McwVerticalLayout* layout, McwWidget* widget){
    if (layout->layout_widgets == NULL){
        layout->layout_widgets = new_mcw_widget_list(1);
        set_mcw_widget_in_mcw_widget_list(layout->layout_widgets, 0, widget);
    }
    else
        append_mcw_widget_to_mcw_widget_list(layout->layout_widgets, widget);
    mcw_set_widget_parent(widget, layout);
};

void mcw_update_vertical_layout(McwVerticalLayout* layout){
    Size size = mcw_get_widget_size(layout->widget.parent);
    
    mcw_set_widget_center_position(MCW_WIDGET(layout), (Point){size.width/2, size.height/2});
    
    int x,y;
    x = size.width/2;
    y = layout->margin;
    
    mcw_set_widget_size(&layout->widget, size);
    
    for (int i = 0; i < get_mcw_widget_list_length(layout->layout_widgets); i++){
        McwWidget* widget = MCW_WIDGET(
            get_mcw_widget_from_mcw_widget_list(layout->layout_widgets, i));
        if (widget == NULL)
            break;
        Size widget_size = mcw_get_widget_size(widget);
        
        mcw_set_widget_center_position(MCW_WIDGET(get_mcw_widget_from_mcw_widget_list(layout->layout_widgets, i)), (Point){x, y + widget_size.height / 2});
        
        y += widget_size.height;
        y += layout->margin; 
    }
//     mcw_set_widget_size(MCW_WIDGET(layout), (Size){2, 3});
};

void mcw_turn_on_layout_trigger_mode(McwVerticalLayout* layout, int boolean){
    layout->trigger_mode = boolean;
};
