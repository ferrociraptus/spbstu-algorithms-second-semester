#ifndef MY_VERTICAL_LAYOUT_H
#define MY_VERTICAL_LAYOUT_H

#include <cairo.h>
#include "McwButton.h"

#define DEFAULT_MYBUTTON_LAYOUT_MARGIN 20

#define SELECT_FRAME_MARGIN 10
#define SELECT_FRAME_COLOR 0, 0.6, 0

typedef struct _McwVerticalLayout McwVerticalLayout;

McwVerticalLayout* mcw_new_vertical_layout();
void mcw_add_widget_to_vertical_layout(McwVerticalLayout* layout, McwWidget* widget);
void mcw_update_vertical_layout(McwVerticalLayout* layout);
void mcw_turn_on_layout_trigger_mode(McwVerticalLayout* layout, int boolean);

#endif
