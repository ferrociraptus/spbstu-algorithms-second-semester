#ifndef POINT_LIST_H
#define POINT_LIST_H

#include "McwWidget.h"

typedef struct _mcw_widget_list VoidList;

McwWidgetList* new_mcw_widget_list(int nodes_amount);
void destroy_mcw_widget_list(McwWidgetList* list);

void clear_mcw_widget_list(McwWidgetList* list);
int insert_mcw_widget_to_mcw_widget_list(McwWidgetList* list, int index, McwWidget* mcw_widget);
int append_mcw_widget_to_mcw_widget_list(McwWidgetList* list, McwWidget* mcw_widget); 
McwWidget* get_mcw_widget_from_mcw_widget_list(McwWidgetList* list, int index);
void set_mcw_widget_in_mcw_widget_list(McwWidgetList* list, int index, McwWidget* mcw_widget); 
void remove_mcw_widget_from_mcw_widget_list(McwWidgetList* list, int index);

unsigned long get_mcw_widget_list_length(McwWidgetList* list);

#endif //POINT_LIST_H
