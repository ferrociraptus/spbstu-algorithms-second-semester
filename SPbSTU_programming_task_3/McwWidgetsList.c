#include "McwWidgetsList.h"
#include <stdlib.h>
#include "McwWidgetsListNode.h"

#define abs(A) (((A) < 0) ? -(A) : (A))

struct _mcw_widget_list{
    unsigned long len;
    unsigned long current_index;
    McwWidgetListNode* current_node;
};

static void __remove_all_nodes(McwWidgetList* list, McwWidgetListNode* node){
    if (node != list->current_node)
        __remove_all_nodes(list, node->next_node);
    mcw_destroy_widget(node->widget);
    free(node);
};

static void __go_to_index(McwWidgetList* list, unsigned index){
    int diff = list->current_index - index;
    list->current_index = index;
    for (int i = 0; i < abs(diff); i++)
        if (diff < 0)
            list->current_node = list->current_node->next_node;
        else
            list->current_node = list->current_node->previous_node;
}

McwWidgetList* new_mcw_widget_list(int nodes_amount){
    McwWidgetList* list = (McwWidgetList*)malloc(sizeof(McwWidgetList));
    if (list == NULL)
        return NULL;
    list->len = 0;
    list->current_index = 0;
    list->current_node = NULL;
    McwWidgetListNode *node;

    if (nodes_amount > 0){
        node = new_mcw_widget_list_node(NULL);
        if (node == NULL)
            return NULL;
        
        list->current_node = node;
        node->previous_node = node;
        node->next_node = node;
        list->len++;
        for (int i = 0; i < nodes_amount - 1; i++){
            node->next_node = new_mcw_widget_list_node(NULL);
            if (node->next_node == NULL){
                node->next_node = list->current_node;
                destroy_mcw_widget_list(list);
                return NULL;
            }
            node->next_node->previous_node = node;
            node = node->next_node;
            list->len++;
        }
        node->next_node = list->current_node;
        list->current_node->previous_node = node;
    }
    return list;
};

void destroy_mcw_widget_list(McwWidgetList* list){
    if (list == NULL)
        return;
    clear_mcw_widget_list(list);
    free(list);
}

void clear_mcw_widget_list(McwWidgetList* list){
    if (list->current_node != NULL)
        __remove_all_nodes(list, list->current_node->next_node);
    list->len = 0;
    list->current_index = 0;
    list->current_node = NULL;
}

int insert_mcw_widget_to_mcw_widget_list(McwWidgetList* list, int index, McwWidget* mcw_widget){
    McwWidgetListNode* new_node = new_mcw_widget_list_node(mcw_widget);
    if (new_node == NULL || index < 0)
        return -1;
    if (index > list->len)
        index = list->len;
    
    if (list->len == 0){
        new_node->next_node = new_node;
        new_node->previous_node = new_node;
        list->current_node = new_node;
    }
    else{
        __go_to_index(list, index);
        new_node->next_node = list->current_node;
        new_node->previous_node = list->current_node->previous_node;
        new_node->previous_node->next_node = new_node;
        list->current_node->previous_node = new_node;
    }
    list->current_index++;
    list->len++;
    return 1;
}

int append_mcw_widget_to_mcw_widget_list(McwWidgetList* list, McwWidget* mcw_widget){
    return insert_mcw_widget_to_mcw_widget_list(list, list->len, mcw_widget);
}

McwWidget* get_mcw_widget_from_mcw_widget_list(McwWidgetList* list, int index){
    __go_to_index(list, index);
    return list->current_node->widget;
}

void set_mcw_widget_in_mcw_widget_list(McwWidgetList* list, int index, McwWidget* mcw_widget){
    __go_to_index(list, index);
    if (list->current_node->widget != NULL)
        mcw_destroy_widget(list->current_node->widget);
    list->current_node->widget = mcw_widget;
}

void remove_mcw_widget_from_mcw_widget_list(McwWidgetList* list, int index){
    __go_to_index(list, index);
    McwWidgetListNode* del_node = list->current_node;
    del_node->previous_node->next_node = del_node->next_node;
    del_node->next_node->previous_node = del_node->previous_node;
    list->current_node = del_node->next_node;
    list->len--;
    list->current_index = list->current_index % list->len;
    destroy_mcw_widget_list_node(del_node);
}

unsigned long get_mcw_widget_list_length(McwWidgetList* list){
    return list->len;
}
