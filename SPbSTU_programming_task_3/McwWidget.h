#ifndef MY_WIDGET_H
#define MY_WIDGET_H

#include <cairo.h>

//mcw - my custom widgets

#define MCW_WIDGET_DRAW_FUN(A) ((void (*)(void*, cairo_t*))(A))
#define MCW_WIDGET_SIGNALS_HANDLER(A) (void (*) (void*, void*))(A)
#define MCW_DESTROY_WIDGET_HANDLER(A) (void (*) (void*))(A)

#define MCW_WIDGET(A) (McwWidget*)(A)
#define MCW_BUTTON(A) (McwButton*)(A)
#define MCW_VERTICAL_LAYOUT(A) (McwVerticalLayout*)(A)

#define MCW_CLICKED_SIGNAL(A) &(((McwWidget*)(A))->signals.clicked)
#define MCW_RELEASE_SIGNAL(A) &(((McwWidget*)(A))->signals.release)
//#define MCW_PRESSED_DOWN_SIGNAL(A) &((McwWidget*)(A)->signals.pressed_down)
//#define MCW_PRESSED_UP_SIGNAL(A) &((McwWidget*)(A)->signals.pressed_up)
//#define MCW_PRESSED_ENTER_SIGNAL(A) &((McwWidget*)(A)->signals.pressed_enter)

#define MCW_ROOT NULL
#define AUTO_SIZE -1
#define INDIFICATOR_SIZE 50

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

typedef struct{
    double x;
    double y;
} Point;

typedef struct{
    int width;
    int height;
} Size;

typedef struct{
    void (*signal) (void*, void*);
    void* signal_data; 
} McwWidgetSignal;

typedef enum {AUTO_OFF ,AUTO_MIN, AUTO_MAX} AutosizeType;

typedef struct{
    AutosizeType height;
    AutosizeType width;
} McwWidgetAutosize;

typedef struct{
    McwWidgetSignal clicked;
    McwWidgetSignal release;
    McwWidgetSignal pressed_down;
    McwWidgetSignal pressed_up;
    McwWidgetSignal pressed_enter;
} McwSygnals;

typedef struct{
    
    void* parent;
    
    unsigned is_visible : 1;
    
    char indificator[INDIFICATOR_SIZE];
    
    Size size;
    Point center_position;
    McwWidgetAutosize autosize;
    
    void (*draw_mywidget) (void*, cairo_t*);
    void (*destroy_widget)(void*);
    
    McwSygnals signals;
    McwSygnals __system_calls;
    
} McwWidget;

void mcw_empty_handler(void*, void*);

McwWidget mcw_create_base_widget();

McwWidget* mcw_new_widget();
void mcw_destroy_widget(McwWidget* widget);
Size mcw_get_widget_size(McwWidget* widget);
void mcw_set_widget_size(McwWidget* widget, Size new_size);
Point mcw_get_widget_center_position(McwWidget* widget);
void mcw_set_widget_center_position(McwWidget* widget, Point new_pos);
void mcw_set_widget_parent(McwWidget* widget, void* parent);
void mcw_draw_widget(McwWidget* widget, cairo_t* cr);
void mcw_set_press_handler_for_widget(McwWidget* widget, void (*)(void*, void*), void* data);
void mcw_set_autosize_scale_for_widget(McwWidget* widget, AutosizeType width, AutosizeType height);

void mcw_rise_signal(McwWidget* widget, McwWidgetSignal* signal);
void mcw_connect_signal_handler_to_widget(McwWidgetSignal* signal, void (*handler) (void*, void*), void* data);

void mcw_set_widget_indificator(McwWidget* widget, char* id);

int mcw_is_widget_visible(McwWidget* widget);

#endif
