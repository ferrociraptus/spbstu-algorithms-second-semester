#include <stdio.h>
#include "McwSystem.h"
#include "mcw.h"


void system_open_close_button_handler(McwButton* button, void* data){
    mcw_set_button_active(button, FALSE);
    mcw_system_close_ui();
    mcw_system_load_ui("StyleSheet2.mcw");
}

void initialize_mybutton_handler(McwButton* button, void* data){
    static unsigned char trigger;
    if (!trigger) {
        mcw_set_new_text_for_button(button, "Realize system");
        trigger = 1;
    } else {
        mcw_set_new_text_for_button(button, "Initialize system");
        trigger = 0;
    }
}

void start(){
    mcw_system_load_ui("StyleSheet.mcw");
    
    McwWidget* button1 = mcw_system_get_widget("button1");
    mcw_connect_signal_handler_to_widget(MCW_CLICKED_SIGNAL(button1), MCW_WIDGET_SIGNALS_HANDLER(initialize_mybutton_handler), NULL);
    
    McwWidget* button2 = mcw_system_get_widget("button2");
    mcw_connect_signal_handler_to_widget(MCW_CLICKED_SIGNAL(button2), MCW_WIDGET_SIGNALS_HANDLER(system_open_close_button_handler), NULL);
    
}

int main(int argc, char** argv){
    mcw_system_connect_init_function(start);
    mcw_system_run(argc, argv);
    mcw_system_close();
}
