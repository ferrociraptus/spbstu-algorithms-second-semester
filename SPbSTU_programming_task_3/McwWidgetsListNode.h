#ifndef MCW_WIDGET_LIST_NODE_H
#define MCW_WIDGET_LIST_NODE_H

#include "McwWidget.h"

typedef struct _list_node {
    struct _list_node* next_node;
    struct _list_node* previous_node;
    McwWidget* item;
} McwWidgetListNode;

McwWidgetListNode* new_mcw_widget_list_node(McwWidget* widget);
void destroy_mcw_widget_list_node(McwWidgetListNode *node);

#endif
