#include <stdlib.h>
#include <string.h>

#include "McwButton.h"
#include "McwWidget.h"

typedef enum {UNPRESSED = 0, PRESSED = 1} McwButtonState;

struct _McwButton {
    McwWidget widget;
    char* text;
    int font_size;
    unsigned char is_active :1;
    McwButtonState state;
};


void __mcw_press_button(McwButton* button){
    if (button->is_active == FALSE)
        return;
    button->state = PRESSED;
    mcw_rise_signal(MCW_WIDGET(button), &button->widget.signals.clicked);
};

void __mcw_unpress_button(McwButton* button){
    if (button->is_active == FALSE)
        return;
    button->state = UNPRESSED;
};


void draw_mybutton(McwButton* button, cairo_t* cr){
    Point up_left_apex;
    Size button_size = mcw_get_widget_size(MCW_WIDGET(button));
    Point button_middle = mcw_get_widget_center_position(MCW_WIDGET(button));
    
    up_left_apex.x = button_middle.x - button_size.width/2;
    up_left_apex.y = button_middle.y - button_size.height/2;
    
    cairo_set_source_rgb(cr, MYBUTTON_UNPRESSED_BACKGROUND_COLOR);
    cairo_rectangle(cr, up_left_apex.x, up_left_apex.y, button_size.width, button_size.height);
    cairo_fill(cr);
    if (button->state == PRESSED){
        if (button->is_active)
            cairo_set_source_rgb(cr, MYBUTTON_PRESSED_BACKGROUND_COLOR);
        else
            cairo_set_source_rgb(cr, MYBUTTON_PASSIVE_COLOR);
        cairo_rectangle(cr, up_left_apex.x, up_left_apex.y, button_size.width - MYBUTTON_PRESS_DRAW_MARGIN, button_size.height - MYBUTTON_PRESS_DRAW_MARGIN);
        cairo_fill(cr);
    }
    
    cairo_set_source_rgb(cr, MYBUTTON_FONT_COLOR);
    cairo_set_font_size(cr, button->font_size);
    
    cairo_font_extents_t fe;
    cairo_text_extents_t te;
    cairo_font_extents (cr, &fe);
    cairo_text_extents(cr, button->text, &te);
    cairo_move_to (cr, button_middle.x - te.x_bearing - te.width / 2,
            button_middle.y - fe.descent + fe.height / 2);
    cairo_show_text (cr, button->text);
    cairo_stroke(cr);
    return;
};

void _mcw_destroy_button(McwButton* button){
    if (button == NULL)
        return;
    if (button->text != NULL)
        free(button->text);
    free(button);
};

McwButton* mcw_new_button(const char* text){
    McwButton* button = (McwButton*)malloc(sizeof(McwButton));
    if (button == NULL)
        return NULL;
    
    int text_len = strlen(text);
    button->text = (char*)malloc(sizeof(char)*text_len + 1);
    
    if (button->text == NULL){
        free(button);
        return NULL;
    }
    memcpy(button->text, text, text_len+1);
    
    button->is_active = TRUE;
    button->state = UNPRESSED;
    button->font_size = MYBUTTON_FONT_SIZE;
    button->widget = mcw_create_base_widget();
    button->widget.draw_mywidget = MCW_WIDGET_DRAW_FUN(draw_mybutton);
    button->widget.size.height = 50;
    button->widget.size.width = 200;
    button->widget.destroy_widget = MCW_DESTROY_WIDGET_HANDLER(_mcw_destroy_button);
    
    button->widget.__system_calls.clicked.signal = MCW_WIDGET_SIGNALS_HANDLER(__mcw_press_button);
    button->widget.__system_calls.release.signal = MCW_WIDGET_SIGNALS_HANDLER( __mcw_unpress_button);
    return button;
};

void mcw_set_new_text_for_button(McwButton* button, char* new_text){
//     free(button->text);
    int text_len = strlen(button->text);
    memset(button->text, '\0', text_len);
    
    text_len = strlen(new_text);
    button->text = (char*)realloc(button->text ,sizeof(char)*text_len + 1);
    
    if (button->text == NULL){
        free(button);
        return;
    }
    memset(button->text, '\0', text_len+1);
    memcpy(button->text, new_text, text_len);
};

void mcw_set_button_active(McwButton* button, int boolean){
    button->is_active = boolean;
};

void mcw_set_button_font_size(McwButton* button, unsigned int size){
    button->font_size = size;
};
