#ifndef MCW_SYSTEM_H
#define MCW_SYSTEM_H

#include "mcw.h"

void mcw_system_connect_init_function(void (*initialization_fun)());
void mcw_system_run(int argc, char** argv);
void mcw_system_load_ui(const char* file_name);
void mcw_system_close_ui();
void mcw_system_close();

McwWidget* mcw_system_get_widget(const char* indificator);

#endif
