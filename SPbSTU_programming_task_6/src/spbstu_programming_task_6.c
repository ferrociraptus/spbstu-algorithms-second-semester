#include <stdio.h>
#include "mcw.h"
#include "DrawApp.h"

void init_fun(){
	draw_application_init();
}

int main(int argc, char** argv){
	mcw_system_connect_init_function(init_fun);
	mcw_system_run(argc, argv);
    return 0;
}
