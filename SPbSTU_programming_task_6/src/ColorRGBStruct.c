#include "ColorRGBStruct.h"

ColorRGB color_create(double red, double green, double blue){
    return (ColorRGB){red, green, blue};
}

ColorRGB color_multiply(ColorRGB color, double val){
    return (ColorRGB){
        .red = color.red * val,
        .green = color.green * val,
        .blue = color.blue * val
    };
}

ColorRGB color_sum(ColorRGB color1, ColorRGB color2){
    return (ColorRGB){
        .red = color1.red + color2.red,
        .green = color1.green + color2.green,
        .blue = color1.blue + color2.blue
    };
}
ColorRGB color_reduce(ColorRGB color1, ColorRGB color2){
    return (ColorRGB){
        .red = color1.red - color2.red,
        .green = color1.green - color2.green,
        .blue = color1.blue - color2.blue
    };
}
