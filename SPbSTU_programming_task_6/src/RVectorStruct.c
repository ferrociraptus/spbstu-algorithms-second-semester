#include <math.h>
#include "RVectorStruct.h"

RVector rvector_create(double x, double y){
    return point_create(x, y);
}

double rvector_len(RVector vector){
    return sqrt(pow(vector.x, 2) + pow(vector.y, 2));
}

RVector rvector_sum(RVector vector1, RVector vector2){
    return point_sum(vector1, vector2);
}

RVector rvector_multiply(RVector vector, double val){
    return (RVector){
        .x = vector.x * val,
        .y = vector.y * val
    };
}

RVector rvector_set_len(RVector vector, double len){
    double coeff = len/rvector_len(vector);
    return rvector_multiply(vector, coeff);
}

int rvector_is_zero(RVector vector){
    return point_is_zero(vector);
}
