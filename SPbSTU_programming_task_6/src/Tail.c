#include <stdlib.h>
#include <stdio.h>
#include "Tail.h"

TailNode* tail_node_new(double radius, Point position, ColorRGB color){
    TailNode* new_tail_node = (TailNode*)malloc(sizeof(TailNode));
    if (new_tail_node == NULL)
        return NULL;
    new_tail_node->radius = radius;
    new_tail_node->position = position;
    new_tail_node->color = color;
    
    return new_tail_node;
}


Tail* tail_new ( unsigned int length, Point start_position, ColorRange colors, double radius, double measure_diff )
{
    Tail* tail = void_list_new(length, free);
    
    if (tail == NULL)
        return NULL;
    
    color_range_set_range(&colors, length);
    
    double rad_diff = (radius*measure_diff - radius)/(double)length;
    double rad_buff = radius;
    for (unsigned i = 0; i < void_list_len(tail); i++){
        void_list_set(tail, i, tail_node_new(rad_buff, start_position, color_range_get_val(colors, i)));
        rad_buff += rad_diff;
    }
//     ((TailNode*)void_list_get(tail, 0))->is_visible = 1;
    return tail;
}

void tail_shift_head(Tail *tail, Point vector){
    Point buff_point = point_sum(((TailNode*)void_list_get(tail, 0))->position, vector);
    for (int i = void_list_len(tail) - 1; i > 0; i--){
        Point point = ((TailNode*)void_list_get(tail, i - 1))->position;
        ((TailNode*)void_list_get(tail, i))->position = point;
    }
    ((TailNode*)void_list_get(tail, 0))->position = buff_point;
}

void tail_change_color_range(Tail* tail, ColorRange colors){
    color_range_set_range(&colors, void_list_len(tail));
    for (unsigned i = 0; i < void_list_len(tail); i++){
        ((TailNode*)void_list_get(tail, i))->color = color_range_get_val(colors, i);
    }
}

void tail_destroy(Tail* tail){
    void_list_destroy(tail);
}

void tail_print(Tail* tail){
    puts("Tail:");
    for (unsigned i = 0; i < void_list_len(tail); i++){
        printf("\tNode %d:\n", i);
        ColorRGB color = ((TailNode*)void_list_get(tail, i))->color;
        printf("\t\tColor: {%f, %f, %f}\n", color.red, color.green, color.blue);
        Point point = ((TailNode*)void_list_get(tail, i))->position;
        printf("\t\tPoint: {%f, %f}\n", point.x, point.y);
    }
}
