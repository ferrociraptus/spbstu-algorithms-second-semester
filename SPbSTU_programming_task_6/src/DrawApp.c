#include <cairo.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gtk/gtk.h>
#include "DrawApp.h"
#include "McwSystem.h"
#include "McwWidget.h"
#include "ColorRGBStruct.h"

typedef enum {BRUSH, LASTIC} PaintTool;

extern cairo_surface_t* drawing_area_surface;
extern GtkWidget* drawing_area;
int update_cursor_flag = 0;

static struct{
	McwWidget widget;
	unsigned is_pressed :1;
	PaintTool tool;
	ColorRGB color;
	Point last_point;
	double line_width;
	cairo_surface_t* cursor_image;
} paint;

void update_cursor(ColorRGB color){
	cairo_t* cr;
    cr = cairo_create(paint.cursor_image);
    cairo_set_source_rgba(cr, 1, 1, 1, 0);
	cairo_rectangle(cr, 0, 0, paint.line_width, paint.line_width);
	cairo_fill(cr);
	cairo_stroke(cr);
    cairo_paint(cr);
	cairo_set_source_rgb(cr, 0, 0, 0);
	cairo_set_line_width(cr, 1);
	cairo_arc(cr, paint.line_width / 2, paint.line_width / 2, paint.line_width / 2, 0, 2*M_PI);
	cairo_fill(cr);
	cairo_stroke(cr);
	cairo_set_source_rgb(cr, color.red, color.green, color.blue);
	cairo_arc(cr, paint.line_width / 2, paint.line_width / 2, paint.line_width / 2-1 , 0, 2*M_PI);
	cairo_fill(cr);
	cairo_stroke(cr);
    cairo_destroy(cr);
	update_cursor_flag = 1;
}

void mouse_move_handler(void* widget, Point* point){
	if (update_cursor_flag){
		gdk_window_set_cursor(gtk_widget_get_window(drawing_area), gdk_cursor_new_from_surface(gdk_display_get_default(), paint.cursor_image, paint.line_width/2, paint.line_width/2));
		update_cursor_flag = 0;
	}
	
	if (paint.is_pressed){
		cairo_t* cr = cairo_create(drawing_area_surface);
		cairo_set_line_width(cr, paint.line_width);
		cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
		cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
		
		switch (paint.tool){
			case BRUSH:
				cairo_set_source_rgb(cr, paint.color.red, paint.color.green, paint.color.blue);
				cairo_move_to(cr, paint.last_point.x, paint.last_point.y);
				cairo_line_to(cr, point->x, point->y);
				break;
			case LASTIC:
				cairo_set_source_rgb(cr, 1, 1, 1);
				cairo_move_to(cr, paint.last_point.x, paint.last_point.y);
				cairo_line_to(cr, point->x, point->y);
				break;
		}
		cairo_stroke(cr);
		cairo_destroy(cr);
	}
	paint.last_point = *point;
}

void mouse_click_handler(void* widget, void* data){
	paint.is_pressed = 1;
	cairo_t* cr = cairo_create(drawing_area_surface);
	cairo_set_line_width(cr, paint.line_width);
	cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
	cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
	cairo_set_line_width(cr, 1);
	
	switch (paint.tool){
			case BRUSH:
				cairo_set_source_rgb(cr, paint.color.red, paint.color.green, paint.color.blue);
				break;
			case LASTIC:
				cairo_set_source_rgb(cr, 1, 1, 1);
				break;
		}
	cairo_arc(cr, paint.last_point.x, paint.last_point.y, paint.line_width / 2, 0, 2*M_PI);
	cairo_fill(cr);
	cairo_stroke(cr);
}

void mouse_relise_handler(void* widget, void* data){
	paint.is_pressed = 0;
}

void brush_button_handler(void* widget, void* data){
	paint.tool = BRUSH;
	update_cursor(paint.color);
}

void lastic_button_handler(void* widget, void* data){
	paint.tool = LASTIC;
	update_cursor(color_create(1, 1, 1));
}

void spinbox_changed_handler(void* widget, void* data){
	paint.line_width = *((int*)data);
	cairo_surface_destroy(paint.cursor_image);
	paint.cursor_image = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, paint.line_width, paint.line_width);
	switch (paint.tool){
			case BRUSH:
				update_cursor(paint.color);
				break;
			case LASTIC:
				update_cursor(color_create(1, 1, 1));
				break;
		}
	
}

static void on_response(GtkDialog *dialog, gint response_id, gpointer user_data)
{
  	GdkRGBA color;
	gtk_color_chooser_get_rgba (GTK_COLOR_CHOOSER(dialog), &color);
	paint.color = color_create(color.red, color.green, color.blue);
	gtk_widget_destroy (GTK_WIDGET(dialog));
}

void color_chooser_handler(void* widget, void* data){
	GtkWidget* color_choose = gtk_color_chooser_dialog_new("Set color", GTK_WINDOW(gtk_widget_get_ancestor(drawing_area, GTK_TYPE_WINDOW)));
	
	g_signal_connect (GTK_DIALOG(color_choose), "response", G_CALLBACK (on_response), NULL);
// 	gtk_window_set_modal (GTK_WINDOW(color_choose), TRUE);
	gtk_dialog_run(GTK_DIALOG(color_choose));
	switch (paint.tool){
			case BRUSH:
				update_cursor(paint.color);
				break;
			case LASTIC:
				update_cursor(color_create(1, 1, 1));
				break;
		}
}

void clear_doc_handler(void* widget, void* data){
	cairo_t* cr = cairo_create(drawing_area_surface);
	cairo_set_source_rgb(cr, 1, 1, 1);
	cairo_fill(cr);
	cairo_stroke(cr);
	cairo_paint(cr);
}

void save_handler(void* widget, void* data){
	GtkWidget *dialog;
	GtkFileChooser *chooser;
	GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_SAVE;
	gint res;

	dialog = gtk_file_chooser_dialog_new ("Save File",
										 GTK_WINDOW(gtk_widget_get_ancestor(drawing_area, GTK_TYPE_WINDOW)),
										action,
										("_Cancel"),
										GTK_RESPONSE_CANCEL,
										("_Save"),
										GTK_RESPONSE_ACCEPT,
										NULL);
	chooser = GTK_FILE_CHOOSER (dialog);
	gtk_file_chooser_set_do_overwrite_confirmation (chooser, TRUE);

// 	if (user_edited_a_new_document)
	gtk_file_chooser_set_current_name (chooser, ("Untitled.png"));
// 	else
// 		gtk_file_chooser_set_filename (chooser, existing_filename);

	res = gtk_dialog_run (GTK_DIALOG(dialog));
	
	if (res == GTK_RESPONSE_ACCEPT){
		char *filename;
		filename = gtk_file_chooser_get_filename(chooser);
		//save
		cairo_surface_write_to_png(drawing_area_surface, filename);
		g_free (filename);
	}

	gtk_widget_destroy (dialog);
}

void load_handler(void* widget, void* data){
	GtkWidget *dialog;
	GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_OPEN;
	gint res;

	dialog = gtk_file_chooser_dialog_new ("Open File",
										GTK_WINDOW(gtk_widget_get_ancestor(drawing_area, GTK_TYPE_WINDOW)),
										action,
										("_Cancel"),
										GTK_RESPONSE_CANCEL,
										("_Open"),
										GTK_RESPONSE_ACCEPT,
										NULL);

	res = gtk_dialog_run (GTK_DIALOG (dialog));
	if (res == GTK_RESPONSE_ACCEPT)
	{
		char *filename;
		GtkFileChooser *chooser = GTK_FILE_CHOOSER (dialog);
		filename = gtk_file_chooser_get_filename (chooser);
		cairo_surface_t* new_png = cairo_image_surface_create_from_png(filename);
		clear_doc_handler(NULL, NULL);
		cairo_t* cr = cairo_create(drawing_area_surface);
		cairo_set_source_surface(cr, new_png, 0, 0);
		cairo_paint(cr);
		cairo_destroy(cr);
		cairo_surface_destroy(new_png);
		g_free (filename);
	}

	gtk_widget_destroy (dialog);
}

void draw_application_init(){
	mcw_system_load_ui("style_sheets/toolbox.mcw");
	paint.widget = mcw_widget_create_base();
	mcw_system_add_widget(&paint.widget);
	
	paint.color = color_create(0, 0, 0);
	paint.is_pressed = 0;
	paint.last_point = point_create(0, 0);
	paint.tool = BRUSH;
	paint.line_width = 1;
	paint.cursor_image = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, paint.line_width, paint.line_width);
	update_cursor(paint.color);

	mcw_widget_connect_signal_handler(&paint.widget.__system_calls.drawing_area_signals.mouse_moved, MCW_WIDGET_SIGNALS_HANDLER(mouse_move_handler), NULL);
	mcw_widget_connect_signal_handler(&paint.widget.__system_calls.drawing_area_signals.clicked, MCW_WIDGET_SIGNALS_HANDLER(mouse_click_handler), NULL);
	mcw_widget_connect_signal_handler(&paint.widget.__system_calls.drawing_area_signals.release, MCW_WIDGET_SIGNALS_HANDLER(mouse_relise_handler), NULL);
	mcw_widget_connect_signal_handler(&paint.widget.__system_calls.spinbox_changed, MCW_WIDGET_SIGNALS_HANDLER(spinbox_changed_handler), NULL);
	
	McwWidget* brush = mcw_system_get_widget("brush");
	mcw_widget_connect_signal_handler(&brush->signals.toolbox_signals.clicked, MCW_WIDGET_SIGNALS_HANDLER(brush_button_handler), NULL);
	
	McwWidget* lastic = mcw_system_get_widget("lastic");
	mcw_widget_connect_signal_handler(&lastic->signals.toolbox_signals.clicked, MCW_WIDGET_SIGNALS_HANDLER(lastic_button_handler), NULL);
	
	McwWidget* color_chooser = mcw_system_get_widget("color");
	mcw_widget_connect_signal_handler(&color_chooser->signals.toolbox_signals.clicked, MCW_WIDGET_SIGNALS_HANDLER(color_chooser_handler), NULL);
	
	McwWidget* clear_doc = mcw_system_get_widget("new");
	mcw_widget_connect_signal_handler(&clear_doc->signals.toolbox_signals.clicked, MCW_WIDGET_SIGNALS_HANDLER(clear_doc_handler), NULL);
	
	McwWidget* save = mcw_system_get_widget("save");
	mcw_widget_connect_signal_handler(&save->signals.toolbox_signals.clicked, MCW_WIDGET_SIGNALS_HANDLER(save_handler), NULL);
	
	McwWidget* load = mcw_system_get_widget("load");
	mcw_widget_connect_signal_handler(&load->signals.toolbox_signals.clicked, MCW_WIDGET_SIGNALS_HANDLER(load_handler), NULL);
}
