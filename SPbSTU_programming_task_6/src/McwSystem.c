#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <math.h>

#include "McwSystem.h"
#include "McwWidgetsList.h"

#define MIN_WINDOW_SIZE_WEIGHT_PX 850
#define MIN_WINDOW_SIZE_HEIGHT_PX 600

#define BACKGROUND_COLOR 1.0, 1.0, 1.0

McwWidgetList* active_window;
static void (*init_fun)() = NULL;

cairo_surface_t* drawing_area_surface = NULL;
static cairo_surface_t* toolbox_area_surface = NULL;
GtkWidget* drawing_area;
static GtkWidget* toolbox_area;
GtkWidget* window;

static void activate(GtkApplication* app, gpointer user_data);
static gboolean redraw_system();
static Size get_drawing_area_size(GtkWidget* area);

void mcw_system_connect_init_function(void (*initialization_fun)()){
    init_fun = initialization_fun;
}

void __mcw_system_check_press(Point position){ 
    McwWidget* pressed_widget;
    
    if (active_window == NULL)
        return;
    
    for (int i = 0; i < mcw_widget_list_len(active_window); i++){
        
        Size widget_size = mcw_widget_get_size(MCW_WIDGET(
            mcw_widget_list_get(active_window, i)));
        
        Point widget_middle = mcw_widget_get_center(MCW_WIDGET(
            mcw_widget_list_get(active_window, i)));
        
        pressed_widget = MCW_WIDGET(mcw_widget_list_get(active_window, i));
        
        if (widget_middle.x - widget_size.width/2 <= position.x &&
                widget_middle.x + widget_size.width/2 >= position.x &&
                widget_middle.y - widget_size.height/2 <= position.y &&
                widget_middle.y + widget_size.height/2 >= position.y)
            mcw_rise_signal(pressed_widget, &pressed_widget->__system_calls.toolbox_signals.clicked);
        
        else
            mcw_rise_signal(pressed_widget, &pressed_widget->__system_calls.toolbox_signals.release);
        }
    }

void mcw_system_run(int argc, char** argv){
    GtkApplication* app;
//     gtk_init(&argc, &argv);
    app = gtk_application_new("org.gtk.example", G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
    g_application_run(G_APPLICATION(app), argc, argv);
    g_object_unref(app);
}

void mcw_system_load_ui(const char* file_name){
    if (active_window == NULL)
        active_window = mcw_widget_list_new(1);
    int tab_counter, previous_tab_amount = 0;
    char str_buf[200];
    
    struct{
        McwWidget* widget;
        unsigned is_layout :1;
    }widget_buf, parent;
    
    widget_buf.is_layout = 0;
    parent.widget = MCW_ROOT;
    parent.is_layout = 0;
    
    unsigned is_parametr = 0;
    
    char str_parse[100];
    char* parse_cursor;

    FILE* ui_file = fopen(file_name, "r");
    if (ui_file == NULL)
        return;
    while (!feof(ui_file)) {
        
        memset(str_buf, '\0', 200);
        str_buf[0] = '#';
        fgets(str_buf, 200, ui_file);
        memset(str_parse, '\0', 100);
        tab_counter = 0;
        
        for (int i = 0; i < 200 && str_buf[i] != '\0'; i++) {
            if (str_buf[i] == '#'){
                tab_counter = previous_tab_amount;
                break;
            }
            switch (str_buf[i]) {
                case '\t':
                    tab_counter ++;
                    if (tab_counter - previous_tab_amount > 0){
                        parent = widget_buf;
                        widget_buf.is_layout = 0;
                    }
                    break;

                case '<':
                    is_parametr = 0;
                    parse_cursor = str_parse;
                    while (str_buf[++i] != '>') {
                        *parse_cursor = str_buf[i];
                        parse_cursor++;
                    }

                    if (strcmp(str_parse, "Widget") == 0)
                        widget_buf.widget = mcw_widget_new();

                    else if (strcmp(str_parse, "Button") == 0)
                        widget_buf.widget = MCW_WIDGET(mcw_new_button(""));

                    else if (strcmp(str_parse, "VerticalLayout") == 0){
                        widget_buf.widget = MCW_WIDGET(mcw_vertical_layout_new());
                        widget_buf.is_layout = 1;
                    }

//                     else if (parent == MCW_ROOT)
//                         mcw_widget_list_set(active_window, 0, widget_buf);
                    break;

                case '$':
                    is_parametr = 1;
                    
                    parse_cursor = str_parse;
                    i++;
                    while (str_buf[i] != '=' && str_buf[i] != ' ') {
                        *parse_cursor = str_buf[i];
                        parse_cursor++;
                        i++;
                    }
                    i++;
                    while (!isalpha(str_buf[i]) && str_buf[i] != '(' && !isdigit(str_buf[i])) i++;
                    
                    int k = i;
                    while (!isspace(str_buf[k])) k++;
                    str_buf[k] = '\0';

                    if (strcmp(str_parse, "name") == 0) {
                        parse_cursor = str_buf + i;
                        while (isalpha(str_buf[i])) i++;
                        str_buf[++i] = '\0';
                        mcw_set_widget_indificator(MCW_WIDGET(widget_buf.widget), parse_cursor);
                    }
                    else if (strcmp(str_parse, "image") == 0) {
						parse_cursor = str_buf + i;
                        mcw_button_load_icon(MCW_BUTTON(widget_buf.widget), parse_cursor);
					}
                    else if (strcmp(str_parse, "text") == 0) {
                        parse_cursor = str_buf + i;
                        while (isalpha(str_buf[i])) i++;
                        str_buf[i] = '\0';
                        mcw_button_set_text(MCW_BUTTON(widget_buf.widget), parse_cursor);
                    }
                    else if (strcmp(str_parse, "absolute_position") == 0) {
                        int x, y;
                        sscanf(str_buf, "%*[$\t a-zA-Z_]=(%d,%d)", &x, &y);
                        mcw_set_widget_center_position(widget_buf.widget, (Point){x, y});
                    }
                    else if (strcmp(str_parse, "size") == 0) {
                        int x, y;
                        sscanf(str_buf, "%*[$\t a-zA-Z_]=(%d,%d)", &x, &y);
                        mcw_widget_set_size(widget_buf.widget, (Size){x, y});
                    }
                    else if (strcmp(str_parse, "font_size") == 0) {
                        int x;
                        sscanf(str_buf, "%*[$\t a-zA-Z_]=%d", &x);
                        mcw_button_set_font_size(MCW_BUTTON(widget_buf.widget), x);
                    }
                    else if (strcmp(str_parse, "is_active") == 0) {
                        int x;
                        sscanf(str_buf, "%*[$\t a-zA-Z_]=%d", &x);
                        mcw_button_set_font_size(MCW_BUTTON(widget_buf.widget), x);
                    }
                    else if (strcmp(str_parse, "trigger") == 0) {
                        int x;
                        sscanf(str_buf, "%*[$\t a-zA-Z_]=%d", &x);
                        mcw_vertical_layout_trigger_mode(MCW_VERTICAL_LAYOUT(widget_buf.widget), x);
                    }
                    break;
            }
        }
        
        if (!is_parametr){
            mcw_widget_set_parent(widget_buf.widget, parent.widget);
            if (parent.widget == MCW_ROOT){
                mcw_widget_list_set(active_window, 0, widget_buf.widget);
                parent = widget_buf;
            }
            else{
                mcw_widget_list_append(active_window, widget_buf.widget);
                if (parent.is_layout)
                    mcw_vertical_layout_add_widget(MCW_VERTICAL_LAYOUT(parent.widget), widget_buf.widget);
            }
        }
        previous_tab_amount = tab_counter;
    }
}

void mcw_system_new_ui(){
    if (active_window == NULL)
        active_window = mcw_widget_list_new(1);
    McwWidget* window = mcw_widget_new();
    mcw_set_widget_indificator(window, "window");
    mcw_widget_list_set(active_window, 0, window);
    Size area_size = get_drawing_area_size(drawing_area);
    mcw_widget_set_size(window, area_size);
    window->center_position = point_create(area_size.width/2, area_size.height/2);
    
}

void mcw_system_close_ui(){
    mcw_widget_list_destroy(active_window);
    active_window = NULL;
}

McwWidget* mcw_system_get_widget(const char* indificator){
    for (int i = 0; i < mcw_widget_list_len(active_window); i++){
        McwWidget* widget = mcw_widget_list_get(active_window, i);
        if (strcmp(widget->indificator, indificator) == 0)
            return widget;
    }
    return NULL;
};

void mcw_system_add_widget(McwWidget* widget){
    if (widget->parent == NULL)
        mcw_widget_set_parent(widget, mcw_widget_list_get(active_window, 0));
    mcw_widget_list_append(active_window, widget);
}

void mcw_system_close(){
    mcw_widget_list_destroy(active_window);
    gtk_window_close (GTK_WINDOW(window));
}

//GTK part

static Size get_drawing_area_size(GtkWidget* area){
    return (Size){
    gtk_widget_get_allocated_width(area),
    gtk_widget_get_allocated_height(area)};
}


static void clear_surface(cairo_surface_t* surface) {
    cairo_t* cr;
    cr = cairo_create(surface);
    cairo_set_source_rgb(cr, BACKGROUND_COLOR);
    cairo_paint(cr);
    cairo_destroy(cr);
}

static gboolean redraw_system(){
    if (active_window == NULL)
        return FALSE;
    
//     mcw_widget_set_size(mcw_widget_list_get(active_window, 0), get_drawing_area_size(drawing_area));
    cairo_t* cr = cairo_create(toolbox_area_surface);
//     clear_surface(drawing_area_surface);
	clear_surface(toolbox_area_surface);
    
    if (active_window != NULL){
        mcw_widget_list_get(active_window, 0)->size = get_drawing_area_size(toolbox_area);
        for (int i = 0; i < mcw_widget_list_len(active_window); i++){
            mcw_widget_draw(mcw_widget_list_get(active_window, i), cr);
        }
    }
/* set watch cursor */
//     cairo_surface_t* surf;
// 	surf = cairo_image_surface_create_from_png("icons/download_48x48.png");
//     cairo_set_source_surface(cr, surf, 0, 0);
//     cairo_stroke(cr);
	cairo_set_source_surface(cr, toolbox_area_surface, 0, 0);
    cairo_paint(cr);
    cairo_destroy(cr);
    gtk_widget_queue_draw(drawing_area);
	gtk_widget_queue_draw(toolbox_area);
    return TRUE;
}

static gboolean configure_event_cb(GtkWidget* widget, GdkEventConfigure* event, gpointer data) {
    if (*((cairo_surface_t**)data))
        cairo_surface_destroy(*((cairo_surface_t**)data));
    *((cairo_surface_t**)data) = gdk_window_create_similar_surface(gtk_widget_get_window(widget),
              CAIRO_CONTENT_COLOR,
              gtk_widget_get_allocated_width(widget),
              gtk_widget_get_allocated_height(widget));

    clear_surface(*((cairo_surface_t**)data));
    return TRUE;
}

static gboolean draw_cb(GtkWidget* widget, cairo_t* cr, gpointer data) {
    cairo_set_source_surface(cr, *((cairo_surface_t**)data), 0, 0);
    cairo_paint(cr);
    return FALSE;
}

static gboolean close_window(gpointer data) {
    if (drawing_area_surface)
        cairo_surface_destroy(drawing_area_surface);
//     mcw_system_close();
    return TRUE;
}


static gboolean drawing_area_mouse_release_event_handler(GtkWidget* widget, GdkEventMotion* event, void* data) {
//     /* paranoia check, in case we haven't gotten a configure event */
//     Point click_pos = {event->x, event->y};
//     check_if_layout_buttons_pressed(data, click_pos, UNPRESSED);
    for (int i = 0; i < mcw_widget_list_len(active_window); i++){
        McwWidget* widget = mcw_widget_list_get(active_window, i);
        mcw_rise_signal(widget, &widget->__system_calls.drawing_area_signals.release);
    }
    return TRUE;
}

static gboolean toolbox_area_mouse_release_event_handler(GtkWidget* widget, GdkEventMotion* event, void* data) {
//     /* paranoia check, in case we haven't gotten a configure event */
//     Point click_pos = {event->x, event->y};
//     check_if_layout_buttons_pressed(data, click_pos, UNPRESSED);
    for (int i = 0; i < mcw_widget_list_len(active_window); i++){
        McwWidget* widget = mcw_widget_list_get(active_window, i);
        mcw_rise_signal(widget, &widget->__system_calls.toolbox_signals.release);
    }
    return TRUE;
}


static gboolean drawing_area_mouse_click_event_handler(GtkWidget* widget, GdkEventButton* event, gpointer data) {
//     /*
//     1 - left mouse button
//     2 - middle mouse button
//     3 - right mouse button
//     */
//     __mcw_system_check_press((Point){event->x, event->y});
	Point point = {
        .x = (int)event->x,
        .y = (int)event->y
    };
	for (unsigned i = 0; i < mcw_widget_list_len(active_window); i++){
        McwWidget* widget = mcw_widget_list_get(active_window, i);
        mcw_rise_position_signal(widget, &widget->__system_calls.drawing_area_signals.clicked, point);
    }
	redraw_system();
	return TRUE;
}

static gboolean toolbox_area_mouse_click_event_handler(GtkWidget* widget, GdkEventButton* event, gpointer data) {
//     /*
//     1 - left mouse button
//     2 - middle mouse button
//     3 - right mouse button
//     */
    __mcw_system_check_press((Point){event->x, event->y});
	redraw_system();
	return TRUE;
}

static gboolean drawing_area_mouse_motion_event_handler(GtkWidget *widget, GdkEventMotion *event, gpointer data){
    Point point = {
        .x = (int)event->x,
        .y = (int)event->y
    };
	
    for (unsigned i = 0; i < mcw_widget_list_len(active_window); i++){
        McwWidget* widget = mcw_widget_list_get(active_window, i);
        mcw_rise_position_signal(widget, &widget->__system_calls.drawing_area_signals.mouse_moved, point);
    }
	redraw_system();
    return TRUE;
}

static gboolean toolbox_area_mouse_motion_event_handler(GtkWidget *widget, GdkEventMotion *event, gpointer data){
    Point point = {
        .x = (int)event->x,
        .y = (int)event->y
    };
    gdk_cursor_new_from_name(gdk_display_get_default(), "default");
    for (unsigned i = 0; i < mcw_widget_list_len(active_window); i++){
        McwWidget* widget = mcw_widget_list_get(active_window, i);
        mcw_rise_position_signal(widget, &widget->__system_calls.toolbox_signals.mouse_moved, point);
    }
    return TRUE;
}

static gboolean on_key_release(GtkWidget* widget, GdkEventKey* event, gpointer data) {
    for (int i = 0; i < mcw_widget_list_len(active_window); i++){
        McwWidget* widget = mcw_widget_list_get(active_window, i);
        switch (event->keyval) {
            case GDK_KEY_KP_Enter:
            case GDK_KEY_Return:
                mcw_rise_signal(widget, &widget->__system_calls.drawing_area_signals.release);
                break;
        }
    }
    return TRUE;
}

gboolean on_key_press(GtkWidget* widget, GdkEventKey* event, gpointer* data) {
    
    for (int i = 0; i < mcw_widget_list_len(active_window); i++){
        McwWidget* widget = mcw_widget_list_get(active_window, i);
        switch (event->keyval) {
            case GDK_KEY_Up:
                mcw_rise_signal(widget, &widget->__system_calls.pressed_up);
                break;
            case GDK_KEY_Down:
                mcw_rise_signal(widget, &widget->__system_calls.pressed_down);
                break;
            case GDK_KEY_KP_Enter:
            case GDK_KEY_Return:
                mcw_rise_signal(widget, &widget->__system_calls.pressed_enter);
                break;
        }
    }
    return TRUE;
}

gboolean spin_button_value_change_signal_handler(GtkSpinButton *spin_button, gpointer data){
	for (unsigned i = 0; i < mcw_widget_list_len(active_window); i++){
        McwWidget* widget = mcw_widget_list_get(active_window, i);
		int val = ((int)gtk_spin_button_get_value(spin_button));
        (*(widget->__system_calls.spinbox_changed.signal))(widget, &val);
    }
    return TRUE;
}

static void activate(GtkApplication* app, gpointer user_data){
    GtkWidget* frame;
	GtkWidget* label_line_width;
	GtkWidget* spin_line_width;
	GtkWidget* toolbox;
	GtkWidget* window_box;

    window = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW(window), "Menu");
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width(GTK_CONTAINER(window), 10);

    GdkRectangle workarea = {0};
    gdk_monitor_get_workarea(gdk_display_get_primary_monitor(gdk_display_get_default()), &workarea);

    GdkGeometry hints;
    hints.min_width = MIN_WINDOW_SIZE_WEIGHT_PX;
    hints.max_width = MIN_WINDOW_SIZE_WEIGHT_PX;
    hints.min_height = MIN_WINDOW_SIZE_HEIGHT_PX;
    hints.max_height = MIN_WINDOW_SIZE_HEIGHT_PX;

    gtk_window_set_geometry_hints(GTK_WINDOW(window), window, &hints, (GdkWindowHints)(GDK_HINT_MIN_SIZE | GDK_HINT_MAX_SIZE));
    g_signal_connect(window, "destroy", G_CALLBACK(close_window), NULL);
    g_signal_connect(G_OBJECT(window), "key_press_event", G_CALLBACK(on_key_press), NULL);
    g_signal_connect(G_OBJECT(window), "key_release_event", G_CALLBACK(on_key_release), NULL);
    g_signal_connect (window, "motion-notify-event",
                    G_CALLBACK (drawing_area_mouse_motion_event_handler), NULL);
    
    drawing_area = gtk_drawing_area_new();
	toolbox_area = gtk_drawing_area_new();
	
	toolbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	window_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
	
	label_line_width = gtk_label_new("Width");
	spin_line_width = gtk_spin_button_new_with_range(1, 100, 0.5);
	gtk_widget_set_size_request(spin_line_width, 10,-1);
	g_signal_connect(spin_line_width, "value-changed", G_CALLBACK(spin_button_value_change_signal_handler), NULL);
	
	gtk_box_pack_start(GTK_BOX(toolbox), toolbox_area, TRUE, TRUE, 0);
	gtk_box_pack_end(GTK_BOX(toolbox), spin_line_width, FALSE, FALSE, 0);
	gtk_box_pack_end(GTK_BOX(toolbox), label_line_width, FALSE, FALSE, 0);
	
	gtk_box_pack_start(GTK_BOX(window_box), toolbox, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(window_box), drawing_area, TRUE, TRUE, 0);

    frame = gtk_frame_new(NULL);
    gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
    gtk_container_add(GTK_CONTAINER(frame), window_box);

    gtk_container_add(GTK_CONTAINER(window), frame);


    g_signal_connect(drawing_area, "draw",
                     G_CALLBACK(draw_cb), &drawing_area_surface);
    g_signal_connect(drawing_area, "configure-event",
                     G_CALLBACK(configure_event_cb), &drawing_area_surface);
	
	g_signal_connect(toolbox_area, "draw",
                     G_CALLBACK(draw_cb), &toolbox_area_surface);
    g_signal_connect(toolbox_area, "configure-event",
                     G_CALLBACK(configure_event_cb), &toolbox_area_surface);

    gtk_widget_add_events(window, GDK_POINTER_MOTION_MASK);


    g_signal_connect(drawing_area, "button-release-event",
                     G_CALLBACK(drawing_area_mouse_release_event_handler), NULL);
    g_signal_connect(drawing_area, "button-press-event",
                     G_CALLBACK(drawing_area_mouse_click_event_handler), NULL);
	
	g_signal_connect(toolbox_area, "button-release-event",
                     G_CALLBACK(toolbox_area_mouse_release_event_handler), NULL);
    g_signal_connect(toolbox_area, "button-press-event",
                     G_CALLBACK(toolbox_area_mouse_click_event_handler), NULL);

    //g_timeout_add(50, redraw, drawing_area);
    /* Ask to receive events the drawing area doesn't normally
    * subscribe to. In particular, we need to ask for the
    * button press and motion notify events that want to handle.
    */
    gtk_widget_set_events(drawing_area, gtk_widget_get_events(drawing_area)
                          | GDK_BUTTON_PRESS_MASK
                          | GDK_POINTER_MOTION_MASK
                          | GDK_BUTTON_RELEASE_MASK);
	
	gtk_widget_set_events(toolbox_area, gtk_widget_get_events(drawing_area)
                          | GDK_BUTTON_PRESS_MASK
                          | GDK_POINTER_MOTION_MASK
                          | GDK_BUTTON_RELEASE_MASK);
//     g_timeout_add(REDRAW_SYSTEM_TIME, G_SOURCE_FUNC(redraw_system), NULL);
// 	gtk_widget_add_tick_callback(window, redraw_system, NULL, NULL);
	gtk_widget_show_all(window);
    if (init_fun != NULL)
        (*init_fun)();
	
	redraw_system();
}

