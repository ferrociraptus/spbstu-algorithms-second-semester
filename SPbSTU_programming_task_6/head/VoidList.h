#ifndef POINT_LIST_H
#define POINT_LIST_H

#define DESTRUCTOR(A) (Destructor)(A)

typedef struct _mcw_void_list VoidList;
typedef void(*Destructor)(void*);


VoidList* void_list_new(int nodes_amount, Destructor items_destructor);
void void_list_destroy(VoidList* list);

void void_list_clear(VoidList* list);
int void_list_insert(VoidList* list, int index, void* item);
int void_list_append(VoidList* list, void* item); 
void* void_list_get(VoidList* list, int index);
void void_list_set(VoidList* list, int index, void* item); 
void void_list_remove(VoidList* list, int index);
void* void_list_pop(VoidList* list, int index);

unsigned long void_list_len(VoidList* list);

#endif //POINT_LIST_H
