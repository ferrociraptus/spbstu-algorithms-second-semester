#ifndef MY_BUTTON
#define MY_BUTTON

#include "McwWidget.h"

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

#define DEFAULT_BOARDER_MARGIN

#define MYBUTTON_PRESSED_BACKGROUND_COLOR 0.2, 0.2, 0.4
#define MYBUTTON_UNPRESSED_BACKGROUND_COLOR 0.6, 0.6, 0.6
#define MYBUTTON_PASSIVE_COLOR 0.3, 0, 0
#define MYBUTTON_FONT_COLOR 0, 0, 0
#define MYBUTTON_PRESS_DRAW_MARGIN 3
#define MYBUTTON_FONT_SIZE 16

typedef struct _McwButton McwButton;


McwButton* mcw_new_button(const char* text);
void mcw_button_set_text(McwButton* button, char* new_text);
void mcw_button_set_active(McwButton* button, int boolean);
void mcw_button_set_font_size(McwButton* button, unsigned int size);
void mcw_button_load_icon(McwButton* button, const char* icon_path);
#endif
