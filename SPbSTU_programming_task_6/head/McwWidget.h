#ifndef MY_WIDGET_H
#define MY_WIDGET_H

#include <cairo.h>
#include "PointStruct.h"

//mcw - my custom widgets

#define MCW_WIDGET_DRAW_FUN(A) ((void (*)(void*, cairo_t*))(A))
#define MCW_WIDGET_SIGNALS_HANDLER(A) (void (*) (void*, void*))(A)
#define MCW_DESTROY_WIDGET_HANDLER(A) (void (*) (void*))(A)

#define MCW_WIDGET(A) (McwWidget*)(A)
#define MCW_BUTTON(A) (McwButton*)(A)
#define MCW_VERTICAL_LAYOUT(A) (McwVerticalLayout*)(A)

#define MCW_CLICKED_SIGNAL(A) &(((McwWidget*)(A))->signals.clicked)
#define MCW_RELEASE_SIGNAL(A) &(((McwWidget*)(A))->signals.release)
//#define MCW_PRESSED_DOWN_SIGNAL(A) &((McwWidget*)(A)->signals.pressed_down)
//#define MCW_PRESSED_UP_SIGNAL(A) &((McwWidget*)(A)->signals.pressed_up)
//#define MCW_PRESSED_ENTER_SIGNAL(A) &((McwWidget*)(A)->signals.pressed_enter)

#define MCW_ROOT NULL
#define AUTO_SIZE -1
#define INDIFICATOR_SIZE 50

#ifndef TRUE
#define TRUE 1
#define FALSE 0
#endif

typedef struct{
    int width;
    int height;
} Size;

typedef struct{
    void (*signal) (void*, void*);
    void* signal_data; 
} McwWidgetSignal;

typedef enum {AUTO_OFF ,AUTO_MIN, AUTO_MAX} AutosizeType;

typedef struct{
    AutosizeType height;
    AutosizeType width;
} McwWidgetAutosize;

typedef struct{
	struct{
		McwWidgetSignal clicked;
		McwWidgetSignal release;
		McwWidgetSignal mouse_moved;
	} toolbox_signals, drawing_area_signals;
    
	McwWidgetSignal pressed_down;
    McwWidgetSignal pressed_up;
    McwWidgetSignal pressed_enter;
	
	McwWidgetSignal spinbox_changed;
} McwSygnals;

typedef struct _mcw_widget{
    
    struct _mcw_widget* parent;
    
    unsigned is_visible : 1;
    
    char indificator[INDIFICATOR_SIZE];
    
    Size size;
    Point center_position;
    McwWidgetAutosize autosize;
    
    void (*draw_mywidget) (void*, cairo_t*);
    void (*destroy_widget)(void*);
    
    McwSygnals signals;
    McwSygnals __system_calls;
    
} McwWidget;

void mcw_empty_handler(void*, void*);

McwWidget mcw_widget_create_base();

McwWidget* mcw_widget_new();
void mcw_widget_destroy(McwWidget* widget);
Size mcw_widget_get_size(McwWidget* widget);
void mcw_widget_set_size(McwWidget* widget, Size new_size);
Point mcw_widget_get_center(McwWidget* widget);
void mcw_set_widget_center_position(McwWidget* widget, Point new_pos);
void mcw_widget_set_parent(McwWidget* widget, void* parent);
void mcw_widget_draw(McwWidget* widget, cairo_t* cr);
void mcw_widget_set_press_handler(McwWidget* widget, void (*)(void*, void*), void* data);
void mcw_set_autosize_scale_for_widget(McwWidget* widget, AutosizeType width, AutosizeType height);

void mcw_rise_signal(McwWidget* widget, McwWidgetSignal* signal);
void mcw_rise_position_signal(McwWidget* widget, McwWidgetSignal* signal, Point position);
void mcw_widget_connect_signal_handler(McwWidgetSignal* signal, void (*handler) (void*, void*), void* data);

void mcw_set_widget_indificator(McwWidget* widget, char* id);

int mcw_widget_is_visible(McwWidget* widget);

#endif
