#include <stdio.h>
#include <string.h>
#include <ctype.h>

typedef unsigned long long counter;

struct {
    unsigned byte_option: 1;
    unsigned line_option: 1;
    unsigned word_option: 1;
} program_options = {0, 0, 0};

typedef struct{
    counter lines;
    counter words;
    counter bytes;
} WcInfo;

typedef enum {IN_WORD, IN_SPASE} ParserState; 

void print_help() {
    puts("Usage: wc [OPTION]... [FILE]...");
    puts("Print newline, word, and byte counts for each FILE, and a total line if");
    puts("more than one FILE is specified.  A word is a non-zero-length sequence of");
    puts("characters delimited by white space.\n");
    puts("With no FILE, or when FILE is -, read standard input.\n");
    puts("The options below may be used to select which counts are printed, always in");
    puts("the following order: newline, word, character, byte, maximum line length.");
    printf("\t%-15s\t- display this help and exit\n", "-?, \\?");
    printf("\t%-15s\t- print the byte counts\n", "-C");
    printf("\t%-15s\t- print the newline counts\n", "-L");
    printf("\t%-15s\t- print the word counts\n", "-W");

}

void get_wc_file_info(FILE* file, WcInfo* info){
    char char_buffer;
    ParserState state = IN_SPASE;
    info->lines = info->words = info->bytes = 0;
    while ((char_buffer = getc(file)) != EOF){
        (info->bytes)++;
        if (isalnum(char_buffer) && state != IN_WORD){
            state = IN_WORD;
            (info->words)++;
        }
        else if (isspace(char_buffer)){
            state = IN_SPASE;
            if (char_buffer == '\n')
                (info->lines)++;
        }
            
    }
}

void print_wc_result(char* file_name, WcInfo info){
    printf("%15s:", file_name);
    if (program_options.line_option)
        printf("\t%llu", info.lines);
    if (program_options.word_option)
        printf("\t%llu", info.words);
    if (program_options.byte_option)
        printf("\t%llu", info.bytes);
    putchar('\n');
}

int main(int argc, char** argv) {
    
    FILE* file;
    
    while(--argc != 0){
        argv++;
        if (strcmp(*argv, "\\?") == 0){
            print_help();
            return 0;
        }
        if (**argv != '-')
            break;
        else{
            char* cursor = *argv + 1;
            for (; *cursor != '\0'; cursor++){
                switch(*cursor){
                    case 'C':
                        program_options.byte_option = 1;
                        break;
                    case 'L':
                        program_options.line_option = 1;
                        break;
                    case 'W':
                        program_options.word_option = 1;
                        break;
                    case '?':
                        print_help();
                        return 0;
                        break;
                    default:
                        fprintf(stderr, "Incorrect input argument -%c\n", *cursor);
                        return 0;
                }
            }
        }
    }
    
    if (program_options.byte_option + program_options.line_option + program_options.word_option == 0){
            program_options.byte_option = 1;
            program_options.line_option = 1;
            program_options.word_option = 1;
    
    }
    
    counter byte_counter, total_byte_counter = 0;
    counter string_counter, total_line_counter = 0;
    counter word_counter, total_word_counter = 0;
    
    WcInfo file_wc_info;
    WcInfo total_file_wc_info = {0, 0, 0};
    
    
    if (argc == 0){
        get_wc_file_info(stdin, &file_wc_info);
        print_wc_result("", file_wc_info);
    }
    else{
        for (int i = 0; i < argc; i++){
                if ((file = fopen(*argv, "r")) == NULL){
                    perror("Error opening the file");
                    return 0;
                }
            get_wc_file_info(file, &file_wc_info);
            print_wc_result(*argv, file_wc_info);
            total_file_wc_info.bytes += file_wc_info.bytes;
            total_file_wc_info.lines += file_wc_info.lines;
            total_file_wc_info.words += file_wc_info.words;
            argv++;
        }
        
    }
    if (argc > 1)
        print_wc_result("Total", file_wc_info);
    return 0;
}
