#include "PointList.h"
#include <stdlib.h>
#include "PointListNode.h"

#define abs(A) (((A) < 0) ? -(A) : (A))

static void __remove_all_nodes(PointList* list, PointListNode* node){
    if (node != list->current_node)
        __remove_all_nodes(list, node->next_node);
    destroy_point(node->point);
    free(node);
};

static void __go_to_index(PointList* list, unsigned index){
    int diff = list->current_index - index;
    list->current_index = index;
    for (int i = 0; i < abs(diff); i++)
        if (diff < 0)
            list->current_node = list->current_node->next_node;
        else
            list->current_node = list->current_node->previous_node;
}

PointList *new_point_list(int nodes_amount){
    PointList* list = (PointList*)malloc(sizeof(PointList));
    if (list == NULL)
        return NULL;
    list->len = 0;
    list->current_index = 0;
    list->current_node = NULL;
    PointListNode *node;

    if (nodes_amount > 0){
        node = new_point_list_node(NULL);
        if (node == NULL)
            return NULL;
        
        list->current_node = node;
        node->previous_node = node;
        node->next_node = node;
        list->len++;
        for (int i = 0; i < nodes_amount - 1; i++){
            node->next_node = new_point_list_node(NULL);
            if (node->next_node == NULL){
                node->next_node = list->current_node;
                destroy_point_list(list);
                return NULL;
            }
            node->next_node->previous_node = node;
            node = node->next_node;
            list->len++;
        }
        node->next_node = list->current_node;
        list->current_node->previous_node = node;
    }
    return list;
};

void destroy_point_list(PointList* list){
    clear_point_list(list);
    free(list);
}

void clear_point_list(PointList* list){
    if (list->current_node != NULL)
        __remove_all_nodes(list, list->current_node->next_node);
    list->len = 0;
    list->current_index = 0;
    list->current_node = NULL;
}

int insert_point_to_point_list(PointList* list, int index, Point* point){
    PointListNode* new_node = new_point_list_node(point);
    if (new_node == NULL || index < 0)
        return -1;
    if (index > list->len)
        index = list->len;
    
    if (list->len == 0){
        new_node->next_node = new_node;
        new_node->previous_node = new_node;
        list->current_node = new_node;
    }
    else{
        __go_to_index(list, index);
        new_node->next_node = list->current_node;
        new_node->previous_node = list->current_node->previous_node;
        new_node->previous_node->next_node = new_node;
        list->current_node->previous_node = new_node;
    }
    list->current_index++;
    list->len++;
    return 1;
}

int append_point_to_point_list(PointList* list, Point* point){
    return insert_point_to_point_list(list, list->len, point);
}

Point* get_point_from_point_list(PointList* list, int index){
    __go_to_index(list, index);
    return list->current_node->point;
}

void set_point_in_point_list(PointList* list, int index, Point* point){
    __go_to_index(list, index);
    if (list->current_node->point != NULL)
        destroy_point(list->current_node->point);
    list->current_node->point = point;
}

void remove_point_from_point_list(PointList* list, int index){
    __go_to_index(list, index);
    PointListNode* del_node = list->current_node;
    del_node->previous_node->next_node = del_node->next_node;
    del_node->next_node->previous_node = del_node->previous_node;
    list->current_node = del_node->next_node;
    list->len--;
    list->current_index = list->current_index % list->len;
    destroy_point_list_node(del_node);
}

unsigned long point_list_length(PointList* list){
    return list->len;
}
