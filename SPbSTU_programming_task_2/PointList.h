#ifndef POINT_LIST_H
#define POINT_LIST_H

#include "PointListNode.h"
#include "Point.h"

typedef struct{
    unsigned long len;
    unsigned long current_index;
    PointListNode* current_node;
} PointList;

PointList* new_point_list(int nodes_amount);
void destroy_point_list(PointList* list);

void clear_point_list(PointList* list);
int insert_point_to_point_list(PointList* list, int index, Point* point);
int append_point_to_point_list(PointList* list, Point* point); 
Point* get_point_from_point_list(PointList* list, int index);
void set_point_in_point_list(PointList* list, int index, Point* point); 
void remove_point_from_point_list(PointList* list, int index);

unsigned long point_list_length(PointList* list);

#endif //POINT_LIST_H
