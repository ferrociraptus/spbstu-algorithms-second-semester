#include "PointListNode.h"
#include <stdlib.h>
#include "Point.h"


PointListNode *new_point_list_node(Point* point){
    PointListNode *node = (PointListNode*) malloc(sizeof(PointListNode));
    if (node == NULL)
        return NULL;
    node->point = point;
    return node;
}

void destroy_point_list_node(PointListNode *node){
    if (node == NULL)
        return;
    destroy_point(node->point);
    free(node);
}
