#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "Point.h"
#include "FractalCurve.h"

#define MIN_WINDOW_SIZE_WEIGHT_PX 600
#define MIN_WINDOW_SIZE_HEIGHT_PX 400

enum MouseClickStatesInfo {
    NONE,
    LEFT_MOUSE_BUTTON_CLICK,
    MIDDLE_MOUSE_BUTTON_CLICK,
    RIGHT_MOUSE_BUTTON_CLICK
} mouse_click_state_info;

static cairo_surface_t* surface = NULL;

FractalCurve* fractal_curve;

static gboolean on_key_press(GtkWidget* widget, GdkEventKey* event, gpointer data) {
    switch (event->keyval) {
        case GDK_KEY_Left:
            gtk_spin_button_spin(GTK_SPIN_BUTTON(data), GTK_SPIN_STEP_BACKWARD, 1);
            break;
        case GDK_KEY_Right:
            gtk_spin_button_spin(GTK_SPIN_BUTTON(data), GTK_SPIN_STEP_FORWARD, 1);
            break;
    }

    return TRUE;
}


static void clear_surface(void) {
    cairo_t* cr;

    cr = cairo_create(surface);
    cairo_set_source_rgb(cr, 1, 1, 1);
    cairo_paint(cr);
    cairo_destroy(cr);
}


static gboolean configure_event_cb(GtkWidget* widget, GdkEventConfigure* event, gpointer data) {
    if (surface)
        cairo_surface_destroy(surface);

    surface = gdk_window_create_similar_surface(gtk_widget_get_window(widget),
              CAIRO_CONTENT_COLOR,
              gtk_widget_get_allocated_width(widget),
              gtk_widget_get_allocated_height(widget));

    clear_surface();
    return TRUE;
}


static gboolean draw_cb(GtkWidget* widget, cairo_t* cr, gpointer data) {
    cairo_set_source_surface(cr, surface, 0, 0);
    cairo_paint(cr);
    return FALSE;
}


static gboolean redraw_fractal_curve(gpointer data) {
    cairo_t* cr;
    Point point;
    /* Paint to the surface, where we store our state */
    cr = cairo_create(surface);
    clear_surface();
    
    if (point_list_length(fractal_curve->list) >= 2){
        point = *get_point_from_point_list(fractal_curve->list, 0);
        cairo_move_to(cr, point.x, point.y);

        cairo_set_source_rgb(cr, 0, 0, 0);

        for (int i = 1; i < fractal_curve->list->len; i++) {
            point = *get_point_from_point_list(fractal_curve->list, i);
            cairo_line_to(cr, point.x, point.y);
        }
        cairo_stroke(cr);
    }
    gtk_widget_queue_draw(data);
    cairo_destroy(cr);
    return TRUE;
}


static gboolean close_window(gpointer data) {
    if (surface)
        cairo_surface_destroy(surface);
    return TRUE;
}


static gboolean mouse_motion_event_handler(GtkWidget* widget, GdkEventMotion* event, gpointer data) {
    /* paranoia check, in case we haven't gotten a configure event */
    if (surface == NULL)
        return FALSE;

    if (mouse_click_state_info == LEFT_MOUSE_BUTTON_CLICK) {
        set_point_in_point_list(fractal_curve->list, 1, new_point(event->x, event->y));
        if (!(event->state & GDK_BUTTON1_MASK)) {
            mouse_click_state_info = NONE;
            gtk_widget_set_sensitive(GTK_WIDGET(data), TRUE);
        }
        redraw_fractal_curve(widget);
    }
    if (mouse_click_state_info == MIDDLE_MOUSE_BUTTON_CLICK) {
        set_point_in_point_list(fractal_curve->list, 1, new_point(event->x, event->y));
        redraw_fractal_curve(widget);
    }
    return TRUE;
}


static gboolean mouse_click_event_handler(GtkWidget* widget, GdkEventButton* event, gpointer data) {
    /*
    1 - left mouse button
    2 - middle mouse button
    3 - right mouse button
    */
    gtk_widget_set_sensitive(GTK_WIDGET(data), FALSE);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(data), 0);

    switch (event->button) {
        case LEFT_MOUSE_BUTTON_CLICK:
            if (mouse_click_state_info == MIDDLE_MOUSE_BUTTON_CLICK) {
                mouse_click_state_info = NONE;
                Point* firt_curve_base_point = copy_point(get_point_from_point_list(fractal_curve->list, 0));
                change_fractal_curve_base(fractal_curve, firt_curve_base_point, new_point(event->x, event->y));
                gtk_widget_set_sensitive(GTK_WIDGET(data), TRUE);
                redraw_fractal_curve(widget);
            } else {
                mouse_click_state_info = LEFT_MOUSE_BUTTON_CLICK;
                change_fractal_curve_base(fractal_curve, new_point(event->x, event->y), new_point(event->x, event->y));
            }
            break;

        case MIDDLE_MOUSE_BUTTON_CLICK:
            mouse_click_state_info = MIDDLE_MOUSE_BUTTON_CLICK;
            change_fractal_curve_base(fractal_curve, new_point(event->x, event->y), new_point(event->x, event->y));
            break;
            
        case RIGHT_MOUSE_BUTTON_CLICK:
            mouse_click_state_info = NONE;
            clear_point_list(fractal_curve->list);
            redraw_fractal_curve(widget);
        default:
            mouse_click_state_info = NONE;
            clear_point_list(fractal_curve->list);
            redraw_fractal_curve(widget);
    }
    return TRUE;
}

static void spin_button_value_change_signal_handler(GtkSpinButton *spin_button, gpointer data){
    change_fractal_curve_iteration(fractal_curve, (int)gtk_spin_button_get_value(spin_button));
    redraw_fractal_curve(data);
}

static void combobox_changed_signal_handler(GtkComboBox *widget, gpointer data){
    const char* id = gtk_combo_box_get_active_id(widget);
    gtk_spin_button_set_value(GTK_SPIN_BUTTON(data), 0);
    if (g_strcmp0(id, "koh") == 0){
        change_fractal_curve_type(fractal_curve, KOH);
        gtk_spin_button_set_range(GTK_SPIN_BUTTON(data), 0, 8);
    }
    if (g_strcmp0(id, "levis") == 0){
        change_fractal_curve_type(fractal_curve, LEVI);
        gtk_spin_button_set_range(GTK_SPIN_BUTTON(data), 0, 17);
    }
    if (g_strcmp0(id, "dragon") == 0){
        change_fractal_curve_type(fractal_curve, DRAGON);
        gtk_spin_button_set_range(GTK_SPIN_BUTTON(data), 0, 17);
    }
    if (g_strcmp0(id, "serpinski") == 0){
        change_fractal_curve_type(fractal_curve, SERP_TRIANGLE);
        gtk_spin_button_set_range(GTK_SPIN_BUTTON(data), 0, 10);
    }
}

static void activate(GtkApplication* app, gpointer user_data) {
    GtkWidget* drawing_area;
    GtkWidget* frame;
    GtkWidget* window;
    GtkWidget* vertical_box;

    GtkWidget* combobox;
    GtkWidget* n_spin_entry;
    GtkWidget* horisontal_box;

    mouse_click_state_info = NONE;
    fractal_curve = new_fractal_curve(KOH);

    window = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW(window), "Fractal curves");
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    gtk_container_set_border_width(GTK_CONTAINER(window), 10);

    GdkRectangle workarea = {0};
    gdk_monitor_get_workarea(gdk_display_get_primary_monitor(gdk_display_get_default()),
                             &workarea);

    GdkGeometry hints;
    hints.min_width = MIN_WINDOW_SIZE_WEIGHT_PX;
    hints.max_width = workarea.width;
    hints.min_height = MIN_WINDOW_SIZE_HEIGHT_PX;
    hints.max_height = workarea.height;

    gtk_window_set_geometry_hints(GTK_WINDOW(window), window, &hints, (GdkWindowHints)(GDK_HINT_MIN_SIZE | GDK_HINT_MAX_SIZE));
    g_signal_connect(window, "destroy", G_CALLBACK(close_window), NULL);

    drawing_area = gtk_drawing_area_new();

    frame = gtk_frame_new(NULL);
    gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);
    gtk_container_add(GTK_CONTAINER(frame), drawing_area);

    GtkAdjustment* adjustment = gtk_adjustment_new(0.0, 0.0, 8.0, 1.0, 100.0, 0.0);
    n_spin_entry = gtk_spin_button_new(adjustment, TRUE, 0);
    gtk_widget_set_size_request(n_spin_entry, 120, -1);
    gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(n_spin_entry), TRUE);
    gtk_widget_set_sensitive(n_spin_entry, FALSE);
    g_signal_connect(n_spin_entry, "value-changed", G_CALLBACK(spin_button_value_change_signal_handler), drawing_area);
    
    combobox = gtk_combo_box_text_new();
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combobox), "koh", "Koh curve");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combobox), "dragon", "Dragon curve");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combobox), "levis", "Levis curve");
    gtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(combobox), "serpinski", "Serpinski triangle");
    gtk_combo_box_set_active_id(GTK_COMBO_BOX(combobox), "koh");
    gtk_widget_set_size_request(combobox, 200, -1);
    g_signal_connect(combobox, "changed", G_CALLBACK(combobox_changed_signal_handler), n_spin_entry);
    
    horisontal_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 2);
    gtk_box_pack_end(GTK_BOX(horisontal_box), n_spin_entry, FALSE, FALSE, 3);
    gtk_box_pack_end(GTK_BOX(horisontal_box), combobox, FALSE, FALSE, 3);
    gtk_box_set_homogeneous(GTK_BOX(horisontal_box), FALSE);
    gtk_box_set_spacing(GTK_BOX(horisontal_box), 10);

    
    vertical_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 2);
    gtk_box_pack_start(GTK_BOX(vertical_box), horisontal_box, FALSE, TRUE, 3);
    gtk_box_pack_start(GTK_BOX(vertical_box), frame, TRUE, TRUE, 3);

    gtk_container_add(GTK_CONTAINER(window), vertical_box);


    g_signal_connect(drawing_area, "draw",
                     G_CALLBACK(draw_cb), NULL);
    g_signal_connect(drawing_area, "configure-event",
                     G_CALLBACK(configure_event_cb), NULL);

    gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);

    gtk_event_controller_key_new(window);
    g_signal_connect(G_OBJECT(window), "key_press_event", G_CALLBACK(on_key_press), n_spin_entry);

    gtk_widget_add_events(window, GDK_POINTER_MOTION_MASK);


    g_signal_connect(drawing_area, "motion-notify-event",
                     G_CALLBACK(mouse_motion_event_handler), n_spin_entry);
    g_signal_connect(drawing_area, "button-press-event",
                     G_CALLBACK(mouse_click_event_handler), n_spin_entry);

    //g_timeout_add(50, redraw, drawing_area);
    /* Ask to receive events the drawing area doesn't normally
    * subscribe to. In particular, we need to ask for the
    * button press and motion notify events that want to handle.
    */
    gtk_widget_set_events(drawing_area, gtk_widget_get_events(drawing_area)
                          | GDK_BUTTON_PRESS_MASK
                          | GDK_POINTER_MOTION_MASK);

    gtk_widget_show_all(window);
}




int main(int argc, char** argv) {
    GtkApplication* app;
    int status;
    gtk_init(&argc, &argv);
    app = gtk_application_new("org.gtk.example", G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
    status = g_application_run(G_APPLICATION(app), argc, argv);
    g_object_unref(app);
    destroy_fractal_curve(fractal_curve);
    return status;
}
